#!/usr/bin/env Rscript

#test for transferrability of the instaGraminoid-derived quantitative resistance estimates across
#populations and herbicide treatments through cross-validation

#usage example: $ Rscript cross_validation.r "$(pwd)"/res/SAMPLE/INPUT/SAMPLE_INPUT.csv "$(pwd)"/res/SAMPLE/OUTPUT

#input
args = commandArgs(trailing = TRUE)
input_file = args[1] #input csv file with the instaGraminoid-derived metrics and the binary survival data merged into 1 datframe
output_dir = args[2] #output directory where to write-out all outputs

#import libraries
library(glmnet)
library(agricolae) #for HSD only
library(ROCR) #for ROC plot only

############################################################################################################
############################################################################################################
############################################################################################################
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ FUNCTIONS START @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #


ADDITIONAL_METRICS <- function(DATA, START_METRIC, END_METRIC) {
	#add metric transformations
		#DELTA
		DAYS = levels(dat$DAY)
		nDAYS = length(DAYS)
		METRICS = DATA[, START_METRIC:END_METRIC]
		nMETRICSPerDay = ncol(METRICS) / nDAYS
		for (i in 1:(nDAYS-1)) {
			for (j in (i+1):nDAYS) {
				DELTA = METRICS[, (((j-1)*nMETRICSPerDay)+1):(j*nMETRICSPerDay)] - METRICS[, (((i-1)*nMETRICSPerDay)+1):(i*nMETRICSPerDay)]
				NAMES = paste("DELTA", substr(DAYS[i],4,nchar(DAYS[i])), substr(DAYS[j],4,nchar(DAYS[j])), substr(colnames(DELTA), start=6, stop=nchar(colnames(DELTA))), sep="")
				colnames(DELTA) = NAMES
				DATA = cbind(DATA, DELTA)
			}
		}

		# #PAIRWISE PRODUCTS
		# METRICS = DATA[, START_METRIC:END_METRIC]
		# for (k in 1:(ncol(METRICS)-1)) {
		# 	for (l in (k+1):ncol(METRICS)) {
		# 		PROD = (METRICS[,k])/100 * METRICS[,l]
		# 		NAME = paste("PROD_", colnames(METRICS)[k], "_X_", colnames(METRICS)[l], sep="")
		# 		NAMES = c(colnames(DATA), NAME)
		# 		DATA = cbind(DATA, PROD)
		# 		colnames(DATA) = NAMES
		# 	}
		# }
	return(DATA)
}

CROSS.VALIDATION <- function(DATA, METRICS, ITERATIONS, FOLDS, NEXPLANATORY="ONE", TYPE="ALL") {
	############################
	###						 ###
	### ONE METRIC AT A TIME ###
	###						 ###
	############################
	if (NEXPLANATORY=="ONE") {
		#model building with one explanatory variable (metrics) at a time ---> glm function
		if (TYPE=="ALL") {
			#10-fold cross-validation logistic regression across all populations and herbicides using 1 untransformed metric at a time
			N=nrow(DATA)
			metric=c(); iter=c(); fold=c(); correct=c(); wrong=c(); performance=c()
			pB = txtProgressBar(min=0, max=(length(METRICS)*ITERATIONS*FOLDS), initial = 0, style=3, width=50)
			for (m in METRICS) {
				for (i in 1:ITERATIONS) {
					DATASET=DATA[sample(1:N, size=N, replace=FALSE),]
					for (f in 1:FOLDS) {
						COUNT_VALID = round(N/FOLDS)
						INDEX_VALID = if (f<FOLDS) ((((f-1)*COUNT_VALID)+1):(f*COUNT_VALID)) else ((((f-1)*COUNT_VALID)+1):N)
						TRAINING = DATASET[setdiff(1:N, INDEX_VALID), ]
						VALIDATE = DATASET[INDEX_VALID, ]
						MODEL = eval(parse(text=paste("glm(SURVI ~ ", m, ", data=TRAINING, family=binomial(link='logit'))", sep="")))
						PREDICTED = predict(MODEL, newdata=VALIDATE)
						CLASSIFICATION = ifelse(PREDICTED > 0.50, 1, 0) #we set the cut-off point at 0.50 - to find a better cut-off point implement cross-validation 10-fold?!
						CORRECT = sum(CLASSIFICATION==VALIDATE$SURVI)
						WRONG = sum(CLASSIFICATION!=VALIDATE$SURVI)
						PERFORMANCE = CORRECT/sum(CORRECT, WRONG)
						# LOGIT = glm(VALIDATE$SURVI ~ PREDICTED, family=binomial(link='logit'))
						# plot(VALIDATE$SURVI ~ PREDICTED)
						# curve(predict(LOGIT, data.frame(PREDICTED=x),type='resp'),add=TRUE,col=2)
						# points(PREDICTED, fitted(LOGIT),pch=20,col=2)
						metric=c(metric, m); iter=c(iter, i); fold=c(fold, f); correct=c(correct, CORRECT); wrong=c(wrong, WRONG); performance=c(performance, PERFORMANCE)
						setTxtProgressBar(pB, length(metric))
					}
				}
			}
			close(pB)
			OUT = data.frame(metric, iter, fold, correct, wrong, performance)
				colnames(OUT) = c("METRIC", "ITER", "FOLD", "CORRECT", "WRONG", "PERFORMANCE")

		} else if (TYPE=="POPULATIONS") {
			#constant training population x 10-fold validation on another population across all herbicides
			POPULATIONS = levels(DATA$POPUL)
			pop1=c(); pop2=c(); metric=c(); iter=c(); fold=c(); correct=c(); wrong=c(); performance=c()
			pB = txtProgressBar(min=0, max=(length(POPULATIONS)*length(POPULATIONS)*length(METRICS)*ITERATIONS*FOLDS), initial = 0, style=3, width=50)
			for (p1 in POPULATIONS) {
				TRAINING = subset(DATA, POPUL==p1)
				for (p2 in POPULATIONS) {
					VALIDATE_MAIN = subset(DATA, POPUL==p2)
					N=nrow(VALIDATE_MAIN)
					for (m in METRICS) {
						for (i in 1:ITERATIONS) {
							VALIDATE_MAIN=VALIDATE_MAIN[sample(1:N, size=N, replace=FALSE),]
							for (f in 1:FOLDS) {
								COUNT_VALID = round(N/FOLDS)
								INDEX_VALID = if (f<FOLDS) ((((f-1)*COUNT_VALID)+1):(f*COUNT_VALID)) else ((((f-1)*COUNT_VALID)+1):N)
								VALIDATE = VALIDATE_MAIN[INDEX_VALID, ]
								MODEL = eval(parse(text=paste("glm(SURVI ~ ", m, ", data=TRAINING, family=binomial(link='logit'))", sep="")))
								PREDICTED = predict(MODEL, newdata=VALIDATE)
								CLASSIFICATION = ifelse(PREDICTED > 0.50, 1, 0) #we set the cut-off point at 0.50 - to find a better cut-off point implement cross-validation 10-fold?!
								CORRECT = sum(CLASSIFICATION==VALIDATE$SURVI)
								WRONG = sum(CLASSIFICATION!=VALIDATE$SURVI)
								PERFORMANCE = CORRECT/sum(CORRECT, WRONG)
								# LOGIT = glm(VALIDATE$SURVI ~ PREDICTED, family=binomial(link='logit'))
								# plot(VALIDATE$SURVI ~ PREDICTED)
								# curve(predict(LOGIT, data.frame(PREDICTED=x),type='resp'),add=TRUE,col=2)
								# points(PREDICTED, fitted(LOGIT),pch=20,col=2)
								pop1=c(pop1, p1); pop2=c(pop2, p2); metric=c(metric, m); iter=c(iter, i); fold=c(fold, f); correct=c(correct, CORRECT); wrong=c(wrong, WRONG); performance=c(performance, PERFORMANCE)
								setTxtProgressBar(pB, length(metric))
							}
						}
					}
				}
			}
			close(pB)
			OUT = data.frame(pop1, pop2, metric, iter, fold, correct, wrong, performance)
			colnames(OUT) = c("TRAIN_POP", "TEST_POP", "METRIC", "ITER", "FOLD", "CORRECT", "WRONG", "PERFORMANCE")

		} else if (TYPE=="HERBICIDES") {
			#constant training population x 10-fold validation on another population across all herbicides
			HERBICIDES = levels(DATA$HERBI)
			her1=c(); her2=c(); metric=c(); iter=c(); fold=c(); correct=c(); wrong=c(); performance=c()
			pB = txtProgressBar(min=0, max=(length(HERBICIDES)*length(HERBICIDES)*length(METRICS)*ITERATIONS*FOLDS), initial = 0, style=3, width=50)
			for (h1 in HERBICIDES) {
				TRAINING = subset(DATA, HERBI==h1)
				for (h2 in HERBICIDES) {
					VALIDATE_MAIN = subset(DATA, HERBI==h2)
					N=nrow(VALIDATE_MAIN)
					for (m in METRICS) {
						for (i in 1:ITERATIONS) {
							VALIDATE_MAIN=VALIDATE_MAIN[sample(1:N, size=N, replace=FALSE),]
							for (f in 1:FOLDS) {
								COUNT_VALID = round(N/FOLDS)
								INDEX_VALID = if (f<FOLDS) ((((f-1)*COUNT_VALID)+1):(f*COUNT_VALID)) else ((((f-1)*COUNT_VALID)+1):N)
								VALIDATE = VALIDATE_MAIN[INDEX_VALID, ]
								MODEL = eval(parse(text=paste("glm(SURVI ~ ", m, ", data=TRAINING, family=binomial(link='logit'))", sep="")))
								PREDICTED = predict(MODEL, newdata=VALIDATE)
								CLASSIFICATION = ifelse(PREDICTED > 0.50, 1, 0) #we set the cut-off point at 0.50 - to find a better cut-off point implement cross-validation 10-fold?!
								CORRECT = sum(CLASSIFICATION==VALIDATE$SURVI)
								WRONG = sum(CLASSIFICATION!=VALIDATE$SURVI)
								PERFORMANCE = CORRECT/sum(CORRECT, WRONG)
								# LOGIT = glm(VALIDATE$SURVI ~ PREDICTED, family=binomial(link='logit'))
								# plot(VALIDATE$SURVI ~ PREDICTED)
								# curve(predict(LOGIT, data.frame(PREDICTED=x),type='resp'),add=TRUE,col=2)
								# points(PREDICTED, fitted(LOGIT),pch=20,col=2)
								her1=c(her1, h1); her2=c(her2, h2); metric=c(metric, m); iter=c(iter, i); fold=c(fold, f); correct=c(correct, CORRECT); wrong=c(wrong, WRONG); performance=c(performance, PERFORMANCE)
								setTxtProgressBar(pB, length(metric))
							}
						}
					}
				}
			}
			close(pB)
			OUT = data.frame(her1, her2, metric, iter, fold, correct, wrong, performance)
				colnames(OUT) = c("TRAIN_HERBI", "TEST_HERBI", "METRIC", "ITER", "FOLD", "CORRECT", "WRONG", "PERFORMANCE")
		} else {print("INVALID ANALYSIS TYPE. CHOOSE FROM 'ALL', 'POPULATIONS' and 'HERBICIDES'"); OUT = 0}
	
	############################
	###						 ###
	### USE ALL THE METRICS  ###
	###						 ###
	############################
	} else if (NEXPLANATORY=="ALL") {
		ALPHA=0.5		#elasticnet mixing at half lasso and RR
		#model building with all available variables (metrics) ---> cv.glm function
		if (TYPE=="ALL") {
			#cross-validation across populations and herbicides
			N=nrow(DATA)
			iter=c(); fold=c(); correct=c(); wrong=c(); performance=c()
			#emp.alive=c(); emp.dead=c()
			pB = txtProgressBar(min=0, max=(ITERATIONS*FOLDS), initial = 0, style=3, width=50)
			for (i in 1:ITERATIONS) {
				DATASET=DATA[sample(1:N, size=N, replace=FALSE),]
				for (f in 1:FOLDS) {
					COUNT_VALID = round(N/FOLDS)
					INDEX_VALID = if (f<FOLDS) ((((f-1)*COUNT_VALID)+1):(f*COUNT_VALID)) else ((((f-1)*COUNT_VALID)+1):N)
					TRAINING = DATASET[setdiff(1:N, INDEX_VALID), ]
					VALIDATE = DATASET[INDEX_VALID, ]
					X_train = eval(parse(text=paste("cbind(", paste(paste("TRAINING$", METRICS, sep=""), collapse=","), ")", sep=""))); colnames(X_train) = METRICS
					# X_train = scale(X_train)
					X_valid = eval(parse(text=paste("cbind(", paste(paste("VALIDATE$", METRICS, sep=""), collapse=","), ")", sep=""))); colnames(X_valid) = METRICS
					# X_valid = scale(X_valid)
					MODEL = glmnet(x=X_train, y=as.factor(TRAINING$SURVI), family="binomial", alpha=ALPHA, nlambda=100)	#alpha=0 RR penalty while aplha=1 LASSO penalty ALSO [L2 is alpha of RR; while, L1 is lambda of LASSO]
					LAMBDAi = MODEL$dev.ratio==max(MODEL$dev.ratio)
					PREDICTED = MODEL$a0[LAMBDAi] + (X_valid%*%MODEL$beta[,LAMBDAi]) #more continuous than p(x) = 1/(1+exp(-(Xb)))
					#PREDICTED = 1/(1+exp(-(MODEL$a0[LAMBDAi] + (X_valid%*%MODEL$beta[,LAMBDAi])))) #almost binary since these are the fitted values!
					CLASSIFICATION = ifelse(PREDICTED > 0.50, 1, 0) #we set the cut-off point at 0.50 - to find a better cut-off point implement cross-validation 10-fold?!
					CORRECT = sum(CLASSIFICATION==VALIDATE$SURVI)
					WRONG = sum(CLASSIFICATION!=VALIDATE$SURVI)
					PERFORMANCE = CORRECT/sum(CORRECT, WRONG)
					#plot logistic curve
						# pred = PREDICTED[,1]
						# LOGIT = glm(VALIDATE$SURVI ~ pred, family=binomial(link='logit'))
						# plot(VALIDATE$SURVI ~ pred)
						# curve(predict(LOGIT, data.frame(pred=x),type='resp'),add=TRUE,col=2)
						# points(pred, fitted(LOGIT),pch=20,col=2)
					#EMP.ALIVE = sum(VALIDATE$SURVI==1); EMP.DEAD = sum(VALIDATE$SURVI==0)
					#emp.alive=c(emp.alive, EMP.ALIVE); emp.dead=c(emp.dead, EMP.DEAD)
					iter=c(iter, i); fold=c(fold, f); correct=c(correct, CORRECT); wrong=c(wrong, WRONG); performance=c(performance, PERFORMANCE)
					setTxtProgressBar(pB, length(iter))
				}
			}
			close(pB)
			# OUT = data.frame(iter, fold, correct, wrong, performance, emp.alive, emp.dead)
			# colnames(OUT) = c("ITER", "FOLD", "CORRECT", "WRONG", "PERFORMANCE", "EMPIRICALLY_ALIVE", "EMPIRICALLY_DEAD")
			OUT = data.frame(iter, fold, correct, wrong, performance)
			colnames(OUT) = c("ITER", "FOLD", "CORRECT", "WRONG", "PERFORMANCE")

		} else if (TYPE=="POPULATIONS") {
			#use one population for training and k-fold validation on another divided into k mutually exclusive groups
			POPULATIONS = levels(DATA$POPUL)
			pop1=c(); pop2=c(); iter=c(); fold=c(); correct=c(); wrong=c(); performance=c()
			pB = txtProgressBar(min=0, max=(length(POPULATIONS)*length(POPULATIONS)*ITERATIONS*FOLDS), initial = 0, style=3, width=50)
			for (p1 in POPULATIONS) {
				TRAINING = subset(DATA, POPUL==p1)
				X_train = eval(parse(text=paste("cbind(", paste(paste("TRAINING$", METRICS, sep=""), collapse=","), ")", sep=""))); colnames(X_train) = METRICS
				# X_train = scale(X_train)
				for (p2 in POPULATIONS) {
					VALIDATE_MAIN = subset(DATA, POPUL==p2)
					N=nrow(VALIDATE_MAIN)
					for (i in 1:ITERATIONS) {
						VALIDATE_MAIN=VALIDATE_MAIN[sample(1:N, size=N, replace=FALSE),]
						for (f in 1:FOLDS) {
							COUNT_VALID = round(N/FOLDS)
							INDEX_VALID = if (f<FOLDS) ((((f-1)*COUNT_VALID)+1):(f*COUNT_VALID)) else ((((f-1)*COUNT_VALID)+1):N)
							VALIDATE = VALIDATE_MAIN[INDEX_VALID, ]
							X_valid = eval(parse(text=paste("cbind(", paste(paste("VALIDATE$", METRICS, sep=""), collapse=","), ")", sep=""))); colnames(X_valid) = METRICS
							# X_valid = scale(X_valid)
							MODEL = glmnet(x=X_train, y=as.factor(TRAINING$SURVI), family="binomial", alpha=ALPHA, nlambda=100)
							#PREDICTED = predict(MODEL, newx=X_valid, type="link", s=LAMBDA)
							LAMBDAi = MODEL$dev.ratio==max(MODEL$dev.ratio)
							PREDICTED = MODEL$a0[LAMBDAi] + (X_valid%*%MODEL$beta[,LAMBDAi])  #more continuous than p(x) = 1/(1+exp(-(Xb)))
							#PREDICTED = 1/(1+exp(-(MODEL$a0[LAMBDAi] + (X_valid%*%MODEL$beta[,LAMBDAi]))))  #almost binary since these are the fitted values!
							CLASSIFICATION = ifelse(PREDICTED > 0.50, 1, 0)
							CORRECT = sum(CLASSIFICATION==VALIDATE$SURVI)
							WRONG = sum(CLASSIFICATION!=VALIDATE$SURVI)
							PERFORMANCE = CORRECT/sum(CORRECT, WRONG)
							#plot logistic curve
								# pred = PREDICTED[,1]
								# LOGIT = glm(VALIDATE$SURVI ~ pred, family=binomial(link='logit'))
								# plot(VALIDATE$SURVI ~ pred)
								# curve(predict(LOGIT, data.frame(pred=x),type='resp'),add=TRUE,col=2)
								# points(pred, fitted(LOGIT),pch=20,col=2)
							pop1=c(pop1, p1); pop2=c(pop2, p2); iter=c(iter, i); fold=c(fold, f); correct=c(correct, CORRECT); wrong=c(wrong, WRONG); performance=c(performance, PERFORMANCE)
							setTxtProgressBar(pB, length(pop1))
						}
					}
				}
			}
			close(pB)
			OUT = data.frame(pop1, pop2, iter, fold, correct, wrong, performance)
			colnames(OUT) = c("TRAIN_POP", "TEST_POP", "ITER", "FOLD", "CORRECT", "WRONG", "PERFORMANCE")

		} else if (TYPE=="HERBICIDES") {
			#use one population for training and k-fold validation on another divided into k exclusive groups
			HERBICIDES = levels(DATA$HERBI)
			her1=c(); her2=c(); iter=c(); fold=c(); correct=c(); wrong=c(); performance=c()
			pB = txtProgressBar(min=0, max=(length(HERBICIDES)*length(HERBICIDES)*ITERATIONS*FOLDS), initial = 0, style=3, width=50)
			for (h1 in HERBICIDES) {
				TRAINING = subset(DATA, HERBI==h1)
				X_train = eval(parse(text=paste("cbind(", paste(paste("TRAINING$", METRICS, sep=""), collapse=","), ")", sep=""))); colnames(X_train) = METRICS
				# X_train = scale(X_train)
				for (h2 in HERBICIDES) {
					VALIDATE_MAIN = subset(DATA, HERBI==h2)
					N=nrow(VALIDATE_MAIN)
					for (i in 1:ITERATIONS) {
						VALIDATE_MAIN=VALIDATE_MAIN[sample(1:N, size=N, replace=FALSE),]
						for (f in 1:FOLDS) {
							COUNT_VALID = round(N/FOLDS)
							INDEX_VALID = if (f<FOLDS) ((((f-1)*COUNT_VALID)+1):(f*COUNT_VALID)) else ((((f-1)*COUNT_VALID)+1):N)
							VALIDATE = VALIDATE_MAIN[INDEX_VALID, ]
							X_valid = eval(parse(text=paste("cbind(", paste(paste("VALIDATE$", METRICS, sep=""), collapse=","), ")", sep=""))); colnames(X_valid) = METRICS
							# X_valid = scale(X_valid)
							#defined this ity-bity function here for tryCatch use so that when glmnet encounters a 0 variance dependent variable the loop does not terminate instead it will just spit out NA's
							GLMNET_OUT <- function(x, y, alpha){
								MODEL = glmnet(x=x, y=y, family="binomial", alpha=alpha, nlambda=100)
								#PREDICTED = predict(MODEL, newx=X_valid, type="link", s=LAMBDA)
								LAMBDAi = MODEL$dev.ratio==max(MODEL$dev.ratio)
								PREDICTED = MODEL$a0[LAMBDAi] + (X_valid%*%MODEL$beta[,LAMBDAi]) #more continuous than p(x) = 1/(1+exp(-(Xb)))
								#PREDICTED = 1/(1+exp(-(MODEL$a0[LAMBDAi] + (X_valid%*%MODEL$beta[,LAMBDAi]))))  #almost binary since these are the fitted values!
								CLASSIFICATION = ifelse(PREDICTED > 0.50, 1, 0)
								CORRECT = sum(CLASSIFICATION==VALIDATE$SURVI)
								WRONG = sum(CLASSIFICATION!=VALIDATE$SURVI)
								PERFORMANCE = CORRECT/sum(CORRECT, WRONG)
								return(data.frame(CORRECT, WRONG, PERFORMANCE))
							}
							glmnet_out = tryCatch({GLMNET_OUT(x=X_train, y=as.factor(TRAINING$SURVI), alpha=ALPHA)}, error=function(e){return(data.frame(CORRECT=NA, WRONG=NA, PERFORMANCE=NA))})
							her1=c(her1, h1); her2=c(her2, h2); iter=c(iter, i); fold=c(fold, f); correct=c(correct, glmnet_out$CORRECT); wrong=c(wrong, glmnet_out$WRONG); performance=c(performance, glmnet_out$PERFORMANCE)
							setTxtProgressBar(pB, length(her1))
						}
					}
				}
			}
			close(pB)
			OUT = data.frame(her1, her2, iter, fold, correct, wrong, performance)
			colnames(OUT) = c("TRAIN_HERBI", "TEST_HERBI", "ITER", "FOLD", "CORRECT", "WRONG", "PERFORMANCE")

		} else {print("INVALID ANALYSIS TYPE. CHOOSE FROM 'ALL', 'POPULATIONS' and 'HERBICIDES'"); OUT = 0}

	} else {print("INVALID NUMBER OF EXPLANATORY VARIABLES TO TEST. CHOOSE FROM 'ONE' or 'ALL'"); OUT = 0}

	return(OUT)
}

PLOT.GRAPHS.SAVE.PREDICTORS <- function(TRAINING_DATA, VALIDATION_DATA, METRICS, training_name="TRAIN", validation_name="VALID") {
	#modelling
	ALPHA=0.5		#elasticnet mixing at half lasso and RR
	X_train = eval(parse(text=paste("cbind(", paste(paste("TRAINING_DATA$", METRICS, sep=""), collapse=","), ")", sep="")))
	colnames(X_train) = METRICS
	X_valid = eval(parse(text=paste("cbind(", paste(paste("VALIDATION_DATA$", METRICS, sep=""), collapse=","), ")", sep="")))
	colnames(X_valid) = METRICS
	MODEL = glmnet(x=X_train, y=as.factor(TRAINING_DATA$SURVI), family="binomial", alpha=ALPHA, nlambda=100)
	LAMBDAi = MODEL$dev.ratio==max(MODEL$dev.ratio)
	PREDICTED = MODEL$a0[LAMBDAi] + (X_valid%*%MODEL$beta[,LAMBDAi])
	CLASSIFICATION = ifelse(PREDICTED > 0.50, 1, 0) #we set the cut-off point at 0.50 - to find a better cut-off point implement cross-validation 10-fold?!
	CORRECT = sum(CLASSIFICATION==VALIDATION_DATA$SURVI)
	WRONG = sum(CLASSIFICATION!=VALIDATION_DATA$SURVI)
	PERFORMANCE = CORRECT/sum(CORRECT, WRONG)

	#plotting
	jpeg(paste(training_name, "x", validation_name, "-SUMMARY_PLOTS.jpeg", sep=""), quality=100, width=1000, height=1000)
		par(mfrow=c(2,2))
		pred = PREDICTED[,1]
		LOGIT = glm(VALIDATION_DATA$SURVI ~ pred, family=binomial(link='logit'))
		plot(VALIDATION_DATA$SURVI ~ pred, main="Logistic Regression Plot", sub=paste("CORRECTLY CLASSIFIED = ", round(PERFORMANCE*100, 2), "%", sep=""))
		curve(predict(LOGIT, data.frame(pred=x),type='resp'),add=TRUE,col=2)
		points(pred, fitted(LOGIT),pch=20,col=2)

		outliersRemoved = pred[!(pred %in% boxplot.stats(pred)$out)]
		if (length(pred)>5000) {shap.pvalue = shapiro.test(pred[1:5000])$p.value} else {shap.pvalue = shapiro.test(pred)$p.value}
		plot(density(outliersRemoved), col="blue", main="Density Plot of Predicted Values", sub=paste("Shapiro-Wilks Test p-value = ", format(shap.pvalue, scientific=T, digits=3), sep=""))
		qqnorm(pred, main="QQ Plot of Predicted Values"); qqline(pred, col="red")
		
		pr = prediction(pred, VALIDATION_DATA$SURVI)
		prf = performance(pr, measure="tpr", x.measure="fpr")
		auc = performance(pr, measure="auc")@y.values[[1]]
		plot(prf, col="red", main="ROC Plot", sub=paste("AUC = ", round(auc,2), sep=""))
	dev.off()

	#save predictors
	beta = as.data.frame(MODEL$beta[,LAMBDAi])
	metric = rownames(beta)
	BETA = data.frame(metric, beta); rownames(BETA) = 1:nrow(BETA); colnames(BETA) = c("metric", "beta")
	BETA = BETA[order(BETA$beta, decreasing=TRUE), ]
	write.csv(BETA, file=paste(training_name, "x", validation_name, "-PREDICTORS.csv", sep=""), row.names=F)
}


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ FUNCTIONS END @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
############################################################################################################
############################################################################################################
############################################################################################################



############################################################################################################
############################################################################################################
############################################################################################################
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ EXECUTION START @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

#import data
dat = read.csv(input_file)

#set output directory
setwd(output_dir)

####################################
###								 ###
### BUILD MAIN DATA FOR ANALYSIS ###
###								 ###
####################################
#order
dat = dat[order(dat$REPLI), ]
dat = dat[order(dat$PLANT), ]
dat = dat[order(dat$HERBI), ]
dat = dat[order(dat$POPUL), ]
dat = dat[order(dat$DAY), ]

#isolate factors per measurement time
ndays = nlevels(dat$DAY)
POPUL = dat$POPUL[1:(nrow(dat)/ndays)]
PLANT = dat$PLANT[1:(nrow(dat)/ndays)]
REPLI = dat$REPLI[1:(nrow(dat)/ndays)]
HERBI = dat$HERBI[1:(nrow(dat)/ndays)]
SURVI = dat$SURVI[1:(nrow(dat)/ndays)]

#merge into a new data set
DATA = data.frame(POPUL, PLANT, REPLI, HERBI, SURVI)

#decompose metrics per measurement time and cbind into the new data set
DAYS = levels(dat$DAY)
METRICS = colnames(dat)[7:ncol(dat)]
for (i in DAYS) {
	SUBSET = subset(dat, DAY==i)
	for (j in METRICS) {
		eval(parse(text=paste("DATA$", i, "_", j, " = SUBSET$", j, sep="")))
	}
}

#add additional metrics
DATA = ADDITIONAL_METRICS(DATA=DATA, START_METRIC=6, END_METRIC=ncol(DATA))

#set infinite values as NA
METRICS_ALL=colnames(DATA)[6:ncol(DATA)]
for (m in METRICS_ALL) {
	eval(parse(text=paste("DATA$", m, "[is.infinite(DATA$", m, ")] = NA", sep="")))
}

#remove missing pots
#DATA = DATA[complete.cases(DATA), ]	#remove rows with at least 1 NA #I'm removing too much!! DARN!
DATA = DATA[DATA$DAY00_AREA>0, ]	#remove empty pots at day 0
# DATA = DATA[DATA$DAY07_AREA!=0, ]	#remove empty pots at day 7
# DATA = DATA[DATA$DAY14_AREA!=0, ]	# remove empty pots at dat 14
DATA = DATA[, colSums(is.na(DATA))==0]

str(DATA)

########################
###					 ###
### CROSS-VALIDATION ###
###					 ###
########################
D = DATA
METRICS_ALL=colnames(DATA)[6:ncol(DATA)]
METRICS_ORIG=METRICS_ALL[grep('DAY', METRICS_ALL)]
ITERATIONS=10
FOLDS=10

#run cross-validations (getting perfect fits here: warnings->"glm.fit: fitted probabilities numerically 0 or 1 occurred")
INDMET_ALL = CROSS.VALIDATION(DATA=D, METRICS=METRICS_ORIG, ITERATIONS=ITERATIONS, FOLDS=FOLDS, NEXPLANATORY="ONE", TYPE="ALL")
INDMET_POP = CROSS.VALIDATION(DATA=D, METRICS=METRICS_ORIG, ITERATIONS=ITERATIONS, FOLDS=FOLDS, NEXPLANATORY="ONE", TYPE="POPULATIONS")
INDMET_HER = CROSS.VALIDATION(DATA=D, METRICS=METRICS_ORIG, ITERATIONS=ITERATIONS, FOLDS=FOLDS, NEXPLANATORY="ONE", TYPE="HERBICIDES")

ALLMET_ALL = CROSS.VALIDATION(DATA=D, METRICS=METRICS_ALL, ITERATIONS=ITERATIONS, FOLDS=FOLDS, NEXPLANATORY="ALL", TYPE="ALL")
ALLMET_POP = CROSS.VALIDATION(DATA=D, METRICS=METRICS_ALL, ITERATIONS=ITERATIONS, FOLDS=FOLDS, NEXPLANATORY="ALL", TYPE="POPULATIONS")
ALLMET_HER = CROSS.VALIDATION(DATA=D, METRICS=METRICS_ALL, ITERATIONS=ITERATIONS, FOLDS=FOLDS, NEXPLANATORY="ALL", TYPE="HERBICIDES")

#sort by performance (perfect fit or perfect fit remedied by this here penalized regression)
AGGREGATE_INDMET_ALL = aggregate(PERFORMANCE ~ METRIC, data=INDMET_ALL, FUN=mean); AGGREGATE_INDMET_ALL = AGGREGATE_INDMET_ALL[order(AGGREGATE_INDMET_ALL$PERFORMANCE, decreasing=T),]
AGGREGATE_INDMET_POP = aggregate(PERFORMANCE ~ METRIC + TRAIN_POP + TEST_POP, data=INDMET_POP, FUN=mean); AGGREGATE_INDMET_POP = AGGREGATE_INDMET_POP[order(AGGREGATE_INDMET_POP$PERFORMANCE, decreasing=T),]
AGGREGATE_INDMET_HER = aggregate(PERFORMANCE ~ METRIC + TRAIN_HERBI + TEST_HERBI, data=INDMET_HER, FUN=mean); AGGREGATE_INDMET_HER = AGGREGATE_INDMET_HER[order(AGGREGATE_INDMET_HER$PERFORMANCE, decreasing=T),]

AGGREGATE_ALLMET_ALL = mean(ALLMET_ALL$PERFORMANCE)
AGGREGATE_ALLMET_POP = aggregate(PERFORMANCE ~ TRAIN_POP + TEST_POP, data=ALLMET_POP, FUN=mean); AGGREGATE_ALLMET_POP = AGGREGATE_ALLMET_POP[order(AGGREGATE_ALLMET_POP$PERFORMANCE, decreasing=T),]
AGGREGATE_ALLMET_HER = aggregate(PERFORMANCE ~ TRAIN_HERBI + TEST_HERBI, data=ALLMET_HER, FUN=mean); AGGREGATE_ALLMET_HER = AGGREGATE_ALLMET_HER[order(AGGREGATE_ALLMET_HER$PERFORMANCE, decreasing=T),]

#ANOVA and HSD
INDMET_ALL$TRT = INDMET_ALL$METRIC; mod = lm(PERFORMANCE~TRT, data=INDMET_ALL)
HSD_INDMET_ALL = HSD.test(mod, trt="TRT")$groups; rm(mod)
INDMET_POP$TRT = paste(INDMET_POP$METRIC, INDMET_POP$TRAIN_POP, INDMET_POP$TEST_POP, sep="x"); mod = lm(PERFORMANCE~TRT, data=INDMET_POP)
HSD_INDMET_POP = HSD.test(mod, trt="TRT")$groups; rm(mod)
INDMET_HER$TRT = paste(INDMET_HER$METRIC, INDMET_HER$TRAIN_HERBI, INDMET_HER$TEST_HERBI, sep="x"); mod = lm(PERFORMANCE~TRT, data=INDMET_HER)
#HSD_INDMET_HER = HSD.test(mod, trt="TRT")$groups; rm(mod)
HSD_INDMET_HER = AGGREGATE_INDMET_HER

HSD_ALLMET_ALL = mean(ALLMET_ALL$PERFORMANCE)
ALLMET_POP$TRT = paste(ALLMET_POP$TRAIN_POP, ALLMET_POP$TEST_POP, sep="x"); mod = lm(PERFORMANCE~TRT, data=ALLMET_POP)
HSD_ALLMET_POP = HSD.test(mod, trt="TRT")$groups; rm(mod)
ALLMET_HER$TRT = paste(ALLMET_HER$TRAIN_HERBI, ALLMET_HER$TEST_HERBI, sep="x"); mod = lm(PERFORMANCE~TRT, data=ALLMET_HER)
HSD_ALLMET_HER = HSD.test(mod, trt="TRT")$groups; rm(mod)

#write out
write.csv(HSD_INDMET_ALL, file="PERFORMANCE-INDIVIDUAL_METRICS-ACROSS_ALL_DATA.csv")
write.csv(HSD_INDMET_POP, file="PERFORMANCE-INDIVIDUAL_METRICS-ACROSS_POPULATIONS.csv")
write.csv(HSD_INDMET_HER, file="PERFORMANCE-INDIVIDUAL_METRICS-ACROSS_HERBICIDES.csv")

write.csv(HSD_ALLMET_ALL, file="PERFORMANCE-ALL_METRICS-ACROSS_ALL_DATA.csv")
write.csv(HSD_ALLMET_POP, file="PERFORMANCE-ALL_METRICS-ACROSS_POPULATIONS.csv")
write.csv(HSD_ALLMET_HER, file="PERFORMANCE-ALL_METRICS-ACROSS_HERBICIDES.csv")

#plot graphs given whole data with cross-validation:
PLOT.GRAPHS.SAVE.PREDICTORS(TRAINING_DATA=DATA, VALIDATION_DATA=DATA, training_name="ALL_POP_HER", METRICS=METRICS_ALL, validation_name="ALL_POP_HER")
for (pop1 in levels(DATA$POPUL)) {
	for (pop2 in levels(DATA$POPUL)) {
		PLOT.GRAPHS.SAVE.PREDICTORS(TRAINING_DATA=subset(DATA, POPUL==pop1), VALIDATION_DATA=subset(DATA, POPUL==pop2), METRICS=METRICS_ALL, training_name=pop1, validation_name=pop2)
	}
}
for (her1 in levels(DATA$HERBI)) {
	for (her2 in levels(DATA$HERBI)) {
		tryCatch(PLOT.GRAPHS.SAVE.PREDICTORS(TRAINING_DATA=subset(DATA, HERBI==her1), VALIDATION_DATA=subset(DATA, HERBI==her2), METRICS=METRICS_ALL, training_name=her1, validation_name=her2), 
			error=function(e) {print(paste0(her1, " x ", her2, " - ERROR!"))})
	}
}


# ############################################
# ###										 ###
# ### EXTRACT FINAL QUANTITATIVE PHENOTYPE ###
# ###					AND 				 ###
# ###	  SAMPLE POOLING WITH DENSITY PLOT   ###
# ###										 ###
# ############################################
# #pooling test on Terbuthylazine (Inverleigh + Urana)
# test = subset(DATA, HERBI=="TERBUTHYLAZINE")
# test$ID = paste(test$POPUL, test$PLANT, sep="-")
# metrics=colnames(test)[6:(ncol(test)-1)]
# X = eval(parse(text=paste("cbind(", paste(paste("test$", metrics, sep=""), collapse=","), ")", sep="")))
# colnames(X) = metrics
# model = glmnet(x=X, y=as.factor(test$SURVI), family="binomial", alpha=0.5, nlambda=100) #alpha=0:RR penalty; alpha=1:Lasso penalty
# lambdai = model$dev.ratio==max(model$dev.ratio)
# pred = model$a0[lambdai] + (X%*%model$beta[,lambdai])
# b = as.data.frame(model$beta[,lambdai])
# m = rownames(b)
# beta = data.frame(m,b); rownames(beta) = 1:nrow(beta); colnames(beta)=c("metrics", "beta")
# beta = beta[order(beta$beta, decreasing=TRUE), ]

# class = ifelse(pred > 0.50, 1, 0)
# right = sum(class==test$SURVI)
# wrong = sum(class!=test$SURVI)
# perfo = right/sum(right, wrong)

# test$pX = pred
# pheno = aggregate(pX ~ ID, data=test, FUN=mean)
# pheno = pheno[order(pheno[,2]), ]
# #plot(density(pheno[,2]))

# n = nrow(pheno)
# p = 5
# pheno$pool = c(rep(1:p, each=floor(n/p)), rep(p, times=n%%p))

# colnames(pheno) = c("PLANT", "p(X)", "POOL")
# write.csv(pheno, file="POOLING PLANTS SAMPLE - TERBUTHYLAZINE.csv")
# write.csv(beta, file="POOLING PLANTS SAMPLE - TERBUTHYLAZINE - model predictors.csv", row.names=FALSE)

# #save desnity plot with pools
# library(ggplot2)
# x = density(pheno$'p(X)')$x
# y = density(pheno$'p(X)')$y
# df = data.frame(x,y)

# i=c()
# for (j in 0:(p-1)) i=c(i, j*(round(n/p)))
# i[1] = 1; i = c(i, n)
# q = pheno$'p(X)'[i]
# q[1] = min(x); q[length(q)] = max(x)

# col = c("#D55E00", "#E69F00", "#009E73", "#56B4E9", "#CC79A7")

# jpeg("POOLING PLANTS SAMPLE - TERBUTHYLAZINE.jpeg", quality=100, width=700, height=500)
# qplot(x,y,data=df,geom="line")+
# 	geom_ribbon(data=subset(df ,x>=q[1] & x<q[2]),aes(ymax=y),ymin=0,fill=col[1],colour=NA,alpha=0.5) +
# 	geom_ribbon(data=subset(df ,x>q[2] & x<q[3]),aes(ymax=y),ymin=0,fill=col[2],colour=NA,alpha=0.5) +
# 	geom_ribbon(data=subset(df ,x>q[3] & x<q[4]),aes(ymax=y),ymin=0,fill=col[3],colour=NA,alpha=0.5) +
# 	geom_ribbon(data=subset(df ,x>q[4] & x<q[5]),aes(ymax=y),ymin=0,fill=col[4],colour=NA,alpha=0.5) +
# 	geom_ribbon(data=subset(df ,x>q[5] & x<=q[6]),aes(ymax=y),ymin=0,fill=col[5],colour=NA,alpha=0.5) +
# 	geom_rect(aes(xmin=10, xmax=15, ymin=0.075, ymax=0.080), fill=col[1]) +
# 	geom_rect(aes(xmin=10, xmax=15, ymin=0.070, ymax=0.075), fill=col[2]) +
# 	geom_rect(aes(xmin=10, xmax=15, ymin=0.065, ymax=0.070), fill=col[3]) +
# 	geom_rect(aes(xmin=10, xmax=15, ymin=0.060, ymax=0.065), fill=col[4]) +
# 	geom_rect(aes(xmin=10, xmax=15, ymin=0.055, ymax=0.060), fill=col[5]) +
# 	annotate("text", x=18, y=c(0.0775, 0.0725, 0.0675, 0.0625, 0.0575), label=paste("Pool", 1:p, sep="")) +
# 	labs(x="Terbuthylazine Resistance", y="Density")
# dev.off()
