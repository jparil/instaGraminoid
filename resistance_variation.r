#!/usr/bin/env Rscript

#(1) describe the differences in resistance per population using binary and continuous resistance measurements
		#(1a) binary - chi-square test (difference between populations per herbicide)
		#(1b) conitnuous - t-test (difference between populations per herbicide)
#(2) describe cross-resistance
		# (2a) structural equation model testing variances and covariances - covariances were standardized which means they are correlations!

#usage example: $ Rscript resistance_variation.r EXP16_ANALYSIS_INPUT.csv "$(pwd)"/OUTPUT

#input
args = commandArgs(trailing = TRUE)
input_file = args[1] #input csv file with the instaGraminoid-derived metrics and the binary survival data merged into 1 datframe
output_dir = args[2] #output directory where to write-out all outputs
	#i.e.
	input_file = "EXP16_ANALYSIS_INPUT.csv"
	output_dir = paste0(getwd(), "/OUTPUT/")


#setup libraries and quantitative_resistance_estimate extraction script
library(glmnet)
library(lme4)
library(ROCR)
source("quantitative_resistance.r")

########################################################################
###################
###				###
### BINARY DATA ###
###				###
###################
#import data
dat = read.csv(input_file)

#use only counts at day 14 rep1 and remove empty pots
d = subset(subset(subset(dat, DAY=="DAY14"), REPLI=="REP1"), AREA>0)
d = droplevels(d)

#chi^2 test for independence
CHISQ = c()
for(h in levels(d$HERBI)){
	DATA = subset(d, HERBI==h)
	TABLE = table(DATA$POPUL, DATA$SURVI)
	CHISQ = c(CHISQ, 
		tryCatch(
		{chisq.test(DATA$POPUL, DATA$SURVI)$p.value},
		error = function(e) {NA})
		)
}
CHISQ = data.frame(levels(d$HERBI), CHISQ)
colnames(CHISQ) = c("HERBI", "p.value")

#resistance table
P.R = aggregate(SURVI~POPUL + HERBI, data=d, FUN=mean)
P.R = P.R[order(P.R$POPUL),]
P.R = P.R[order(P.R$HERBI),]

#merge resistance proportion and chi^2 test p-values
OUT1= data.frame(unique(P.R$HERBI))
OUT1 = as.data.frame(cbind(OUT1, matrix(P.R$SURVI, ncol=2, byrow=T)))
colnames(OUT1) = c("HERBI", levels(P.R$POPUL))
OUT1 = merge(OUT1, CHISQ, by="HERBI")

#write out
write.csv(OUT1, file=paste0(output_dir, "/BINARY_DATA-ChisquareTest.csv"))
########################################################################

########################################################################
########################################
###									 ###
### instaGraminoid QUANTITATIVE DATA ###
###									 ###
########################################
#import libraries and quantitaive resistance script
library(glmnet)
library(ggplot2)
source("quantitative_resistance.r")

#import data
dat = read.csv(input_file)

#estimate herbicide resistance as a continuous phenotype using the instaGraminoid photograph-based metrics modelled as the explanatory variables for the binary survival data
DATA = QUANTITATIVE_RESISTANCE_ESTIMATE(dat=dat)

# t-test of populations per herbicide
T = c(); M = c()
for(h in levels(DATA$HERBI)){
	df = subset(DATA, HERBI==h)
	df=droplevels(df)
	T = c(T, 
		tryCatch(
		{t.test(df$QUANT.RES~df$POPUL)$p.value},
		error = function(e) {NA})
		)
	M = c(M, max(df$QUANT.RES))

}
T = data.frame(levels(DATA$HERBI), T, M)
colnames(T) = c("HERBI", "p.value", "max")
sig = c()
for(i in T$p.value) {if(i<0.001) {sig=c(sig, "***")} else if(i<0.01) {sig=c(sig, "**")} else if(i<0.05) {sig=c(sig, "*")} else {sig=c(sig, "ns")}}
T$sig = sig
write.csv(T, file=paste0(output_dir, "/QUANTI_DATA-tTest.csv"))

#violon plots
dT = rbind(T,T)
dT$POPULATION = rep(levels(DATA$POPUL), each=nrow(T)/2)

DATA$POPULATION = DATA$POPUL
dodge <- position_dodge(width = 0.4)
dp <- ggplot(DATA, aes(x=HERBI, y=QUANT.RES, fill=POPULATION)) + 
  geom_violin(trim=FALSE, position=dodge)+
  geom_boxplot(width=0.1, position=dodge)+
  labs(x="HERBICIDE", y = "RESISTANCE ESTIMATE")+
  geom_text(data=dT, aes(x=HERBI, y=max, label=sig), col="black", size=7)+
  theme(text=element_text(size=20), axis.text=element_text(size=20), legend.position=c(0.9, 0.1))

jpeg(paste0(output_dir, "/instaGraminoid_violin_quantResistance.jpeg"), quality=100, width=1200, height=800)
dp
dev.off()
########################################################################

########################################################################
###################################
###								###
### PATTERN OF CROSS-RESISTANCE ###
###								###
###################################
#import libraries and quantitaive resistance script
library(Hmisc)
library(ggplot2)
library(lme4)
library(sem)
source("quantitative_resistance.r")

#import data
dat = read.csv(input_file)

########################
### BINARY DATA
########################
HERBI = levels(dat$HERBI)
sub = subset(dat, HERBI==HERBI[1])
agg = aggregate(SURVI ~ PLANT, data=sub, FUN=min)
for (i in HERBI[2:length(HERBI)]){
	sub = subset(dat, HERBI==i)
	agg = merge(agg, aggregate(SURVI ~ PLANT, data=sub, FUN=min),by="PLANT")
}
colnames(agg) = c("PLANT", HERBI)

#individual R
R1 = data.frame(levels(dat$HERBI), colSums(agg[,2:5])/nrow(agg))
rownames(R1) = NULL
colnames(R1) = c("herbi", "R")
#pair R
R2 = c()
herbi = c()
for (i in 1:(length(HERBI)-1)){
	for (j in (i+1):length(HERBI)){
		herbi = c(herbi, paste0(HERBI[i], "x", HERBI[j]))
		R2 = c(R2, sum((agg[,i+1]*agg[,j+1]))/nrow(agg)) #+1 to exclude the first column "PLANT"
	}
}
R2 = data.frame(herbi, R2)
colnames(R2) = c("herbi", "R")
#3-way R
R3 = c()
herbi = c()
for (i in 1:(length(HERBI)-2)){
	for (j in (i+1):(length(HERBI)-1)){
		for (k in (j+1):length(HERBI)){
			herbi = c(herbi, paste0(HERBI[i], "x", HERBI[j], "x", HERBI[k]))
			R3 = c(R3, sum((agg[,i+1]*agg[,j+1]*agg[,k+1]))/nrow(agg)) #+1 to exclude the first column "PLANT"
		}
	}
}
R3 = data.frame(herbi, R3)
colnames(R3) = c("herbi", "R")
OUT = rbind(R1, R2, R3)
write.csv(OUT, file=paste0(output_dir, "/BINARY_DATA_cross-resistance.csv"))

########################
### QUANTITATIVE DATA
########################
#estimate herbicide resistance as a continuous phenotype using the instaGraminoid photograph-based metrics modelled as the explanatory variables for the binary survival data
DATA = QUANTITATIVE_RESISTANCE_ESTIMATE(dat=dat)

#Structural Equation modelling
#extracting BLUEs of each genotype per herbicide:
for(herbi in levels(DATA$HERBI)){
	blup.mod = lmer(QUANT.RES ~ 0 + (1|REPLI) + PLANT, data=subset(DATA, HERBI==herbi))
	blue = data.frame(fixef(blup.mod))
	rownames(blue) = gsub(pattern="PLANT", replacement="", rownames(blue))
	colnames(blue) = herbi
	eval(parse(text=paste(herbi, "=blue")))
}
m1 = merge(SULFO, TERBU, by="row.names"); rownames(m1) = m1$Row.names; m1 = m1[,2:3]
m2 = merge(GLYPH, TRIFL, by="row.names"); rownames(m2) = m2$Row.names; m2 = m2[,2:3]
MERGED = merge(m1, m2,  by="row.names")
colnames(MERGED) = c("PLANT", "SULFO", "TERBU", "GLYPH", "TRIFL")


#SEM.DAT = PCA.MERGE[,3:6]
SEM.DAT = MERGED[,2:5]
COV = cov(SEM.DAT)
	
	#regression + variance SEM
# MOD.GLYPH=specifyModel()
# 	GLYPH<->GLYPH,VAR.gly,NA #variances
# 	SULFO<->SULFO,VAR.sul,NA
# 	TERBU<->TERBU,VAR.ter,NA
# 	TRIFL<->TRIFL,VAR.tri,NA
# 	GLYPH <-SULFO,REG.glysul,NA #gly
# 	GLYPH <-TERBU,REG.glyter,NA
# 	GLYPH <-TRIFL,REG.glytri,NA

# fit.GLYPH = sem(MOD.GLYPH, COV, nrow(SEM.DAT))
# summary(fit.GLYPH)

# MOD.SULFO=specifyModel()
# 	GLYPH<->GLYPH,VAR.gly,NA #variances
# 	SULFO<->SULFO,VAR.sul,NA
# 	TERBU<->TERBU,VAR.ter,NA
# 	TRIFL<->TRIFL,VAR.tri,NA
# 	SULFO <-GLYPH,REG.sulgly,NA #sul
# 	SULFO <-TERBU,REG.sulter,NA
# 	SULFO <-TRIFL,REG.sultri,NA

# fit.SULFO = sem(MOD.SULFO, COV, nrow(SEM.DAT))
# summary(fit.SULFO)

# MOD.TERBU=specifyModel()
# 	GLYPH<->GLYPH,VAR.gly,NA #variances
# 	SULFO<->SULFO,VAR.sul,NA
# 	TERBU<->TERBU,VAR.ter,NA
# 	TRIFL<->TRIFL,VAR.tri,NA
# 	TERBU <-SULFO,REG.tersul,NA #ter
# 	TERBU <-GLYPH,REG.tergly,NA
# 	TERBU <-TRIFL,REG.tertri,NA

# fit.TERBU = sem(MOD.TERBU, COV, nrow(SEM.DAT))
# summary(fit.TERBU)

# MOD.TRIFL=specifyModel()
# 	GLYPH<->GLYPH,VAR.gly,NA #variances
# 	SULFO<->SULFO,VAR.sul,NA
# 	TERBU<->TERBU,VAR.ter,NA
# 	TRIFL<->TRIFL,VAR.tri,NA
# 	TRIFL <-SULFO,REG.trisul,NA #tri
# 	TRIFL <-TERBU,REG.triter,NA
# 	TRIFL <-GLYPH,REG.trigly,NA

# fit.TRIFL = sem(MOD.TRIFL, COV, nrow(SEM.DAT))
# summary(fit.TRIFL)

# FITS = ls()[grep('fit', ls())]
# lab=c(); est=c(); pval=c()
# for (f in FITS){
# 	f = eval(parse(text=f))
# 	s = summary(f)$coef
# 	lab = c(lab, rownames(s)[5:7])
# 	est = c(est, standardizedCoefficients(f)[5:7,2])
# 	pval = c(pval, s[5:7, 4])
# }

	#variance covariance SEM
MOD=specifyModel()
	GLYPH<->GLYPH,VAR.gly,NA #variances
	SULFO<->SULFO,VAR.sul,NA
	TERBU<->TERBU,VAR.ter,NA
	TRIFL<->TRIFL,VAR.tri,NA
	GLYPH<->SULFO,COV.glysul,NA #covariances
	GLYPH<->TERBU,COV.glyter,NA
	GLYPH<->TRIFL,COV.glytri,NA
	SULFO<->TERBU,COV.sulter,NA
	SULFO<->TRIFL,COV.sultri,NA
	TERBU<->TRIFL,COV.tertri,NA

fit = sem(MOD, COV, nrow(SEM.DAT))
lab=c(); est=c(); pval=c()
summary(fit)
	f = fit
	s = summary(f)$coef
	lab = c(lab, rownames(s)[5:10])
	est = c(est, standardizedCoefficients(f)[5:10,2])
	pval = c(pval, s[5:10, 4])


SEM.OUT = data.frame(lab, est, pval)
write.csv(SEM.OUT, file=paste0(output_dir, "/SEM_regression.csv"))

########################################################################
