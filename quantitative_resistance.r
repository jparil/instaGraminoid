#!/usr/bin/env Rscript

#import libraries
library(glmnet)
library(lme4)
library(ROCR)

#import data
# dat = read.csv("EXP16_ANALYSIS_INPUT.csv")

QUANTITATIVE_RESISTANCE_ESTIMATE <- function(dat) {
	#order
	dat = dat[order(dat$REPLI), ]
	dat = dat[order(dat$PLANT), ]
	dat = dat[order(dat$HERBI), ]
	dat = dat[order(dat$POPUL), ]
	dat = dat[order(dat$DAY), ]

	#isolate factors per measurement time
	ndays = nlevels(dat$DAY)
	POPUL = dat$POPUL[1:(nrow(dat)/ndays)]
	PLANT = dat$PLANT[1:(nrow(dat)/ndays)]
	REPLI = dat$REPLI[1:(nrow(dat)/ndays)]
	HERBI = dat$HERBI[1:(nrow(dat)/ndays)]
	SURVI = dat$SURVI[1:(nrow(dat)/ndays)]

	#merge into a new data set
	DATA = data.frame(POPUL, PLANT, REPLI, HERBI, SURVI)

	#decompose metrics per measurement time and cbind into the new data set
	DAYS = levels(dat$DAY)
	METRICS = colnames(dat)[7:ncol(dat)]
	for (i in DAYS) {
		SUBSET = subset(dat, DAY==i)
		for (j in METRICS) {
			eval(parse(text=paste("DATA$", i, "_", j, " = SUBSET$", j, sep="")))
		}
	}

	#add additional metrics
	ADDITIONAL_METRICS <- function(DATA, START_METRIC, END_METRIC) {
		#add metric transformations
			#DELTA
			DAYS = levels(dat$DAY)
			nDAYS = length(DAYS)
			METRICS = DATA[, START_METRIC:END_METRIC]
			nMETRICSPerDay = ncol(METRICS) / nDAYS
			for (i in 1:(nDAYS-1)) {
				for (j in (i+1):nDAYS) {
					DELTA = METRICS[, (((j-1)*nMETRICSPerDay)+1):(j*nMETRICSPerDay)] - METRICS[, (((i-1)*nMETRICSPerDay)+1):(i*nMETRICSPerDay)]
					NAMES = paste("DELTA", substr(DAYS[i],4,nchar(DAYS[i])), substr(DAYS[j],4,nchar(DAYS[j])), substr(colnames(DELTA), start=6, stop=nchar(colnames(DELTA))), sep="")
					colnames(DELTA) = NAMES
					DATA = cbind(DATA, DELTA)
				}
			}

			# #PAIRWISE PRODUCTS
			# METRICS = DATA[, START_METRIC:END_METRIC]
			# for (k in 1:(ncol(METRICS)-1)) {
			# 	for (l in (k+1):ncol(METRICS)) {
			# 		PROD = (METRICS[,k])/100 * METRICS[,l]
			# 		NAME = paste("PROD_", colnames(METRICS)[k], "_X_", colnames(METRICS)[l], sep="")
			# 		NAMES = c(colnames(DATA), NAME)
			# 		DATA = cbind(DATA, PROD)
			# 		colnames(DATA) = NAMES
			# 	}
			# }
		return(DATA)
	}

	DATA = ADDITIONAL_METRICS(DATA=DATA, START_METRIC=6, END_METRIC=ncol(DATA))

	#set infinite values as NA
	METRICS_ALL=colnames(DATA)[6:ncol(DATA)]
	for (m in METRICS_ALL) {
		eval(parse(text=paste("DATA$", m, "[is.infinite(DATA$", m, ")] = NA", sep="")))
	}

	#remove missing pots
	#DATA = DATA[complete.cases(DATA), ]	#remove rows with at least 1 NA #I'm removing too much!! DARN!
	DATA = DATA[DATA$DAY00_AREA>0, ]	#remove empty pots at day 0
	# DATA = DATA[DATA$DAY07_AREA!=0, ]	#remove empty pots at day 7
	# DATA = DATA[DATA$DAY14_AREA!=0, ]	# remove empty pots at dat 14
	DATA = DATA[, colSums(is.na(DATA))==0]

	metrics=colnames(DATA)[6:ncol(DATA)]
	X = eval(parse(text=paste("cbind(", paste(paste("DATA$", metrics, sep=""), collapse=","), ")", sep="")))
	# X = scale(X)
	colnames(X) = metrics
	model = glmnet(x=X, y=as.factor(DATA$SURVI), family="binomial", alpha=0.5, nlambda=100) #alpha=0:RR penalty; alpha=1:Lasso penalty
	lambdai = model$dev.ratio==max(model$dev.ratio)
	#misc to see the deviance x lambda plot do: mod.cv = cv.glmnet(x=X, y=as.factor(DATA$SURVI), family="binomial", alpha=0.5, nlambda=100); plot(mod.cv)
	QUANT.RES = model$a0[lambdai] + (X%*%model$beta[,lambdai])[,1]
	DATA$PRED = 1/(1+exp(-QUANT.RES))
	DATA$QUANT.RES = (QUANT.RES - min(QUANT.RES))/(max(QUANT.RES) - min(QUANT.RES)) #transform so as to range from 0 to 1
	# #transform back fo logit binary survival prediction via:
	# y = (DATA$QUANT.RES * (max(QUANT.RES) - min(QUANT.RES))) + min(QUANT.RES)
	# surv = 1/(1+exp(-y))
	return(DATA)
}