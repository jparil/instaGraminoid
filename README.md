# instaGraminoid
## A low-cost, unbiased, high-throughput and extensible colorimetric protocol for quantifying herbicide resistance of individual plants

A suite of Bash, python, and R scripts for extracting plant health-related metrics from plant photographs.

*Stanalone platform-specific executables for Linux, Mac and Windows operating systems are available [here](https://gitlab.com/jeffersonfparil/instagraminoid_executables.git).*

## Workflow

<img src="/res/instaGraminoid_method.jpg" width="100%">
<img src="/res/instaGraminoid_scripts.jpg" width="100%">

### Installation

    cd ~
    git clone https://gitlab.com/jeffersonfparil/instaGraminoid.git

Checks for the following dependencies and installs them (This may take a while. Sorry.)
- python
- python-pip
- opencv-python
- numpy
- matplotlib
- scipy
- R
- agricolae
- glmnet
- ROCR
- parallel

Note: Make sure you have updated your system's package repository list

### Usage:

**Arguments:**

|Short Form|Long Form|Description|
|:----:|:------:|:-------------------|
|-h|--help     |show manual|
|-a|--ACTION   |0 for colorimetric analysis of photographs|
|  |           |1 for logistic regression cross-validation and for final quantitative plant health metric extraction|
|-d|--directory|specify the directory where:|
|  |           |for ACTION=0: full path to the photographs|
|  |           |for ACTION=1: full path to the output directory|
|-p|           |specify additional parameters:|
|  |           |FOR ACTION=0:|
|  |           |i) extension name or format of the images (e.g. *jpg*, *tiff*  and *png*)|
|  |           |ii) white pixel value of background: up to 255 (pure white)|
|  |           |iii) plant detection threshold: 0 (less) - 255 (more) |
|  |           |FOR ACTION=1:|
|  |           |i) dataframe of ACTION=0 output merged with the empirical survival data in *csv* format|
|  |           |ii) analysis to perform:|
|  |           |     a) 1 = cross-validation|
|  |           |     b) 2 = logistic regression plots|
|  |           |     c) 3 = quantitative resistance estimates + resistance variations|

**Examples:**

    nohup ./instaGraminoid.sh -a 0 -d ~/instaGraminoid/test -p jpg 200 150 &

    nohup ./instaGraminoid.sh -a 1 -d ~/instaGraminoid/test/OUTPUT -p ~/instaGraminoid/test/SAMPLE_INPUT.csv 1 &

**Usage of miscellaneous scripts:**

    ./rename_pictures.sh /working/directory/ 07

    ./identify_plants.r /output/directory/ OUTPUT.csv SURVIVAL.csv 4 42 3 4 2 2

**Note:**
Refer to the scripts for more details.

## Sample photographs

This is a sample healthy plant:

<img src="/res/DAY14-TRAY03-A1-REP1-HISTOGRAMS.png" width="50%">

This is a sample susceptible plant:

<img src="/res/DAY14-TRAY15-C2-REP1-HISTOGRAMS.png" width="50%">

Each plant is photographed 4 times by turning them 90&deg; sideways to constitute 4 technical replications.

The photography set-up aims to have a uniformly lit area to minimize shadows. The white background is used for white-balance/color correction.

![SETUP1](/res/Photobox.jpg)

![SETUP2](/res/Lolium_rigidum1.jpg)

![SETUP3](/res/Lolium_rigidum2.jpg)

# Citing these magnificent dependencies

- Free Software Foundation. GNU bash [Internet]. 2016. Available: https://www.gnu.org/software/bash/
- Tange O. GNU Parallel - The Command-Line Power Tool. The USENIX Magazine; 2011. pp. 42–47
- R Core Team. R: A Language and Environment for Statistical Computing [Internet]. Vienna, Austria; 2018. Available: https://www.r-project.org/
- Python Software Foundation. Python Language Reference [Internet]. 2018. Available: http://www.python.org
- OpenCV. Open Source Computer Vision Library [Internet]. 2017. Available: https://github.com/itseez/opencv
- Jones E, Oliphant T, Peterson P, Others. SciPy: Open source scientific tools for Python [Internet]. Available: http://www.scipy.org/
- Friedman J, Hastie T, Tibshirani R. Regularization Paths for Generalized Linear Models via Coordinate Descent. Journal of Statistical Software. 2010;33: 1–22. Available: http://www.jstatsoft.org/v33/i01/
- Sing T, Sander O, Beerenwinkel N, Lengauer T. ROCR: visualizing classifier performance in R. Bioinformatics. Oxford University Press; 2005;21: 3940–3941. doi:10.1093/bioinformatics/bti623
