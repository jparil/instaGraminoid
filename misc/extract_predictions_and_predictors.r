#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

# be sure to install the following packages in R: "glmnet", "agricolae", and ROCR"
# sample run: $ nohup ./analyze_METRIC.r OUTPUT.csv SURVIVAL.csv 4 42 3 4 2 2 FALSE &

######################################
###								   ###
### EXTRACT  LOGISTIC  REGRESSION  ###
###		PREDICTIED QUANTITATIVE	   ###
###		  SURVIVAL DATA ANALOG	   ###
###								   ###
######################################

#import libraries
library("glmnet")
library("ROCR")

#inputs
DATA = read.csv(args[1])	#"DATA.csv" output of analyze_METRIC.r
METHOD = args[2]			#"glm" or "glmnet"
METRIC = args[3]			#metric to use (e.g. "DAY14_GREEN_FRACTION") or NULL" for glmnet

ID = paste(DATA$POPUL, DATA$PLANT, sep="-")

if (METHOD=="glm") {
	model = eval(parse(text=paste("glm(SURVI~", METRIC, ", data=DATA, family=binomial(link='logit'))", sep="")))
	pred = eval(parse(text=paste("1/(1+exp(-(model$coef[1] + (DATA$", METRIC, " * model$coef[2]))))", sep="")))
	class = ifelse(pred > 0.5, 1, 0); correct = sum(class==DATA$SURVI)
	perfo = correct/length(class)
	jpeg("Prediction plots - glm.jpeg", quality=100, width=1000, height=1000)
		par(mfrow=c(2,2))
		logit = glm(DATA$SURVI ~ pred, family=binomial(link='logit'))
		plot(DATA$SURVI ~ pred, main="Logistic Regression Plot", sub=paste("CORRECTLY CLASSIFIED = ", round(perfo*100, 2), "%", sep=""))
		curve(predict(logit, data.frame(pred=x),type='resp'),add=TRUE,col=2)
		points(pred, fitted(logit),pch=20,col=2)
		outliersRemoved = pred[!(pred %in% boxplot.stats(pred)$out)]
		if (length(pred)>5000) {shap.pvalue = shapiro.test(pred[1:5000])$p.value} else {shap.pvalue = shapiro.test(pred)$p.value}
		plot(density(outliersRemoved), col="blue", main="Density Plot of Predicted Values", sub=paste("Shapiro-Wilks DATA p-value = ", format(shap.pvalue, scientific=T, digits=3), sep=""))
		qqnorm(pred, main="QQ Plot of Predicted Values"); qqline(pred, col="red")
		pr = prediction(pred, DATA$SURVI)
		prf = performance(pr, measure="tpr", x.measure="fpr")
		auc = performance(pr, measure="auc")@y.values[[1]]
		plot(prf, col="red", main="ROC Plot", sub=paste("AUC = ", round(auc,2), sep=""))
	dev.off()
	pred = data.frame(ID, pred)
	write.csv(pred, "Predicted quantitative herbicide resistance data - glm.csv")
	write.csv(model$coef, "Predictors - glm.csv")
} else if (METHOD=="glmnet") {
	metrics=colnames(DATA)[7:(ncol(DATA))]
	X = eval(parse(text=paste("cbind(", paste(paste("DATA$", metrics, sep=""), collapse=","), ")", sep="")))
	colnames(X) = metrics
	model = glmnet(x=X, y=as.factor(DATA$SURVI), family="binomial", alpha=0.5, nlambda=100) #alpha=0:RR penalty; alpha=1:Lasso penalty
	lambdai = model$dev.ratio==max(model$dev.ratio)
	pred = 1/(1+exp(-(model$a0[lambdai] + (X%*%model$beta[,lambdai]))))
	b = as.data.frame(model$beta[,lambdai])
	m = rownames(b)
	beta = data.frame(m,b); rownames(beta) = 1:nrow(beta); colnames(beta)=c("metrics", "beta")
	beta = beta[order(beta$beta, decreasing=TRUE), ]
	class = ifelse(pred > 0.50, 1, 0)
	right = sum(class==DATA$SURVI)
	wrong = sum(class!=DATA$SURVI)
	perfo = right/sum(right, wrong)
	jpeg("Prediction plots - glmnet.jpeg", quality=100, width=1000, height=1000)
		par(mfrow=c(2,2))
		logit = glm(DATA$SURVI ~ as.matrix(pred), family=binomial(link='logit'))
		plot(DATA$SURVI ~ pred, main="Logistic Regression Plot", sub=paste("CORRECTLY CLASSIFIED = ", round(perfo*100, 2), "%", sep=""))
		curve(predict(logit, data.frame(pred=x),type='resp'),add=TRUE,col=2)
		points(pred, fitted(logit),pch=20,col=2)
		outliersRemoved = pred[!(pred %in% boxplot.stats(pred)$out)]
		if (length(pred)>5000) {shap.pvalue = shapiro.test(pred[1:5000])$p.value} else {shap.pvalue = shapiro.test(pred)$p.value}
		plot(density(outliersRemoved), col="blue", main="Density Plot of Predicted Values", sub=paste("Shapiro-Wilks DATA p-value = ", format(shap.pvalue, scientific=T, digits=3), sep=""))
		qqnorm(pred, main="QQ Plot of Predicted Values"); qqline(pred, col="red")
		pr = prediction(pred, DATA$SURVI)
		prf = performance(pr, measure="tpr", x.measure="fpr")
		auc = performance(pr, measure="auc")@y.values[[1]]
		plot(prf, col="red", main="ROC Plot", sub=paste("AUC = ", round(auc,2), sep=""))
	dev.off()
	pred = data.frame(ID, pred)
	write.csv(pred, "Predicted quantitative herbicide resistance data - glmnet.csv")
	write.csv(beta, "Predictors - glmnet.csv")
} else {
	print("INVALID METHOD!\nChoose from: glm or glmnet\nDefine the metric for glm and set NULL for glmnet")
}