


library(agricolae)


#DEBUGGING:
#load openCV python concatenated output
dat = read.csv("OUTPUT.csv")
#load binary survival or resistance data --->>> scp into the server
x = read.csv("SURVIVAL.csv")
#add necesary factors
r=4		#reps per plant
p=42	#plants per tray
tph=3	#trays per population per herbicide
hp=4	#herbicides per population
pop=2	#populations
d=2		#measurement days

#############################
###						  ###
### ADD NECESSARY FACTORS ###
###						  ###
#############################
dat$POPUL = rep(rep(rep(c("INVERLEIGH", "URANA"), each=(r*p*tph)), times=hp), times=(d))
dat$POPUL = as.factor(dat$POPUL)
dat$PLANT = rep(rep(rep(rep(1:(p*tph), each=r), times=pop), times=hp), times=d)
dat$PLANT = as.factor(dat$PLANT)
dat$HERBI = rep(rep(c("CONTROL", "SULFOMETURON", "TERBUTHYLAZINE", "TRIFLURALIN"), each=(r*p*tph*pop)), times=d)
dat$HERBI = as.factor(dat$HERBI)
dat$SURVI = rep(x$SURVIVAL, times=2)
str(dat)

####################################
###								 ###
### BUILD MAIN DATA FOR ANALYSIS ###
###								 ###
####################################
#isolate factors per measurement time
POPUL = dat$POPUL[1:(nrow(dat)/2)]
PLANT = dat$PLANT[1:(nrow(dat)/2)]
REPLI = dat$REP[1:(nrow(dat)/2)]
HERBI = dat$HERBI[1:(nrow(dat)/2)]
SURVI = x$SURVIVAL

#merge into a new data set
DATA = data.frame(POPUL, PLANT, REPLI, HERBI, SURVI)

#decompose metrics per measurement time and cbind into the new data set
DAYS = levels(dat$DAY)
METRICS = colnames(dat)[5:(ncol(dat)-4)]
for (i in DAYS) {
	SUBSET = subset(dat, DAY==i)
	for (j in METRICS) {
		eval(parse(text=paste("DATA$", i, "_", j, " = SUBSET$", j, sep="")))
	}
}

METRICS = colnames(DATA)[6:ncol(DATA)]

#remove missing pots
DATA = DATA[complete.cases(DATA), ]	#remove rows with at least 1 NA
DATA = DATA[DATA$DAY07_AREA!=0, ]	#remove empty pots at day 7
DATA = DATA[DATA$DAY14_AREA!=0, ]	# remove empty pots at dat 14

# #scale and center (IS THIS REALLY REQUIRED?!?!?!) 20180227 --> NOPE! SAME OUPUT REGARDLESS OF SCALING/CENTERING 20180302
# if (s==TRUE) {
# 	START_METRIC = 6
# 	END_METRIC = ncol(DATA)
# 	DATA[, START_METRIC:END_METRIC] = scale(DATA[, START_METRIC:END_METRIC], center=T, scale=T)
# 	#should we also filter-out highly correlated metrics?
# }

str(DATA)

########################
###					 ###
###   BOOTSTRAPPING  ###
###					 ###
########################


RESIST = subset(DATA, SURVI==1)
SUSCEP = subset(DATA, SURVI==0)

N=700 #since we only have a maximum of 776 susceptible instances
PROP=seq(0,1,by=0.05)
ITERATIONS=10
#m="DAY14_GREEN_FRACTION"
m="DAY14_GREENESS2"

iter=c(); train_resistProp=c(); valid_resistProp=c(); correct=c(); wrong=c(); performance=c()

pB = txtProgressBar(min=0, max=(ITERATIONS*length(PROP)*length(PROP)), initial = 0, style=3, width=50)

for (i in 1:ITERATIONS) {
	for (train in PROP) {
		resist.T=RESIST[sample(1:nrow(RESIST), size=N, replace=FALSE),]
		suscep.T=SUSCEP[sample(1:nrow(SUSCEP), size=N, replace=FALSE),]
		T = rbind(resist.T[1:round(train*N), ], suscep.T[1:round((1-train)*N), ])
		for (valid in PROP) {
			resist.V=RESIST[sample(1:nrow(RESIST), size=N, replace=FALSE),]
			suscep.V=SUSCEP[sample(1:nrow(SUSCEP), size=N, replace=FALSE),]
			V = rbind(resist.V[1:round(valid*N), ], suscep.V[1:round((1-valid)*N), ])
			TRAINING = T
			VALIDATE = V
			MODEL = eval(parse(text=paste("glm(SURVI ~ ", m, ", data=TRAINING, family=binomial(link='logit'))", sep="")))
			PREDICTED = predict(MODEL, newdata=VALIDATE)
			CLASSIFICATION = ifelse(PREDICTED > 0.50, 1, 0) #we set the cut-off point at 0.50 - to find a better cut-off point implement cross-validation 10-fold?!
			CORRECT = sum(CLASSIFICATION==VALIDATE$SURVI)
			WRONG = sum(CLASSIFICATION!=VALIDATE$SURVI)
			PERFORMANCE = CORRECT/sum(CORRECT, WRONG)
			iter=c(iter, i); train_resistProp=c(train_resistProp, train); valid_resistProp = c(valid_resistProp, valid); correct=c(correct, CORRECT); wrong=c(wrong, WRONG); performance=c(performance, PERFORMANCE)
			setTxtProgressBar(pB, length(iter))
		}
	}
}
close(pB)
OUT = data.frame(iter, train_resistProp, valid_resistProp, correct, wrong, performance)
	colnames(OUT) = c("ITER", "TRAINING_RESISTPROP", "VALIDATION_RESISTPROP","CORRECT", "WRONG", "PERFORMANCE")

#some visualizaations
OUT$DIFF = abs(OUT$TRAINING_RESISTPROP - OUT$VALIDATION_RESISTPROP)
boxplot(OUT$PERFORMANCE ~ OUT$DIFF)

out1 = aggregate(PERFORMANCE ~ TRAINING_RESISTPROP + VALIDATION_RESISTPROP, data=OUT, FUN=mean)
out2 = matrix(out1$PERFORMANCE, nrow=length(PROP), byrow=F)
rownames(out2) = PROP
colnames(out2) = PROP
contour(out2)

#write out
write.csv(OUT, file="Dispcrepancies in performance due to distribution of resistants-suceptibles test.csv")

jpeg("Boxplot of Performance x Differences in Resistants Distribution Between Training and Validation Sets.jpeg", quality=100, width=1000, height=800)
par(mar=c(5,5,4,2), cex.lab=2, cex.axis=1.5)
boxplot(OUT$PERFORMANCE ~ OUT$DIFF, xlab="| P(R) in the Training Set - P(R) in the Validation Set |", ylab="Performance")
dev.off()

jpeg(" Contour Plot of Performance x Differences in Resistants Distribution Between Training and Validation Sets.jpeg", quality=100, width=800, height=800)
contour(out2, xlab="Training Prop. Resistants", ylab="Validation Prop. Resistants")
dev.off()

library(plotly)
n = length(PROP)
x = matrix(rep(PROP, each=n), nrow=n, byrow=FALSE)
y = matrix(rep(PROP, each=n), nrow=n, byrow=TRUE)
z = out2
plot_ly(x=x,y=y,z=z,type="surface")