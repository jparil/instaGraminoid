#!/bin/bash

#add herbicide treatment column into the output files

HERBI=$1 #e.g. CONTR, GLYPH, SULFO, TERBU or TRIFL
DAY=$2 #e.g. D00, D07 or D14

DIR=/mnt/EXP16/${HERBI}/OUTPUT/${DAY}/
cd $DIR

echo HERBI > col.temp

for i in $(seq 2 $(wc -l OUTPUT.csv | cut -d" " -f1))
do
echo $HERBI >> col.temp
done

paste -d, col.temp OUTPUT.csv > ${HERBI}-${DAY}-OUTPUT.csv

rm col.temp OUTPUT.csv

#consolidating across herbicides and days
cd /mnt/EXP16
head -n1 CONTR/OUTPUT/D00/*-OUTPUT.csv > CONSOLIDATED-OUTPUT.csv
for out in $(ls */OUTPUT/D*/*-OUTPUT.csv)
do
lines=$(echo $(wc -l $out | cut -d' ' -f1) - 1 | bc)
tail -n$lines $out >> CONSOLIDATED-OUTPUT.csv
done
