#!/usr/bin/env python
import os, sys
import cv2
import numpy as np
from matplotlib import pyplot as plt

#import the image
#filename="INPUT/TRAY01-D5-E5-F5-REP02.JPG"
#filename="INPUT/TRAY24-F7-REP4.jpg"
filename = sys.argv[1]
img = cv2.imread(filename)
nrows, ncols, nchannels = img.shape
filename = filename.split("/")
filename = filename[len(filename)-1]

#crop
#img = img[600:2400,:]

#white balance (and color correction)
def __WHITE_BALANCE__(IMAGE, SET_WHITE=255, PLOT=False):
	mean_column = np.mean(IMAGE[0:200,0:200], axis=0) #140:300, 2:220
	mean_row = np.mean(mean_column, axis=0)
	CORRECTION_FACTOR = SET_WHITE/mean_row #setting the white styrofoam box color to 140-140-140
	IMAGE = np.uint8(CORRECTION_FACTOR*IMAGE)
	if PLOT==True:
		plt.imshow(cv2.cvtColor(IMAGE, cv2.COLOR_BGR2RGB)); plt.show()
	return(IMAGE)

setWhite = 200
img = __WHITE_BALANCE__(IMAGE=img, SET_WHITE=setWhite, PLOT=False)

#detect plants
def __DETECT_PLANTS__(IMAGE, THRESHOLD, COLOR_COLLAPSE="gray", PLOT=False):
	if COLOR_COLLAPSE=="gray":
		COLLAPSED_IMAGE = cv2.cvtColor(IMAGE, cv2.COLOR_BGR2GRAY)
	elif COLOR_COLLAPSE=="blue":
		COLLAPSED_IMAGE = IMAGE[:,:,0]
	elif COLOR_COLLAPSE=="green":
		COLLAPSED_IMAGE = IMAGE[:,:,1]
	elif COLOR_COLLAPSE=="red":
		COLLAPSED_IMAGE = IMAGE[:,:,2]
	else:
		print("That is not a valid COLOR_COLLAPSE input. Choose from 'gray', 'blue', 'green' and 'red'.")
	thresh, mask1 = cv2.threshold(COLLAPSED_IMAGE, THRESHOLD, 255, cv2.THRESH_BINARY_INV)
	kernel = np.ones((5,5),np.uint8)
	mask2 = cv2.morphologyEx(mask1, cv2.MORPH_OPEN, kernel)
	mask3 = cv2.morphologyEx(mask2, cv2.MORPH_CLOSE, kernel)
	mask4, CONTOURS, hierarchy = cv2.findContours(mask3, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	IMAGE_EXTRACT = cv2.bitwise_and(IMAGE, IMAGE, mask=mask4)
	if PLOT==True:
			plt.imshow(COLLAPSED_IMAGE, cmap='gray'); plt.show()
			plt.imshow(mask1, cmap='gray'); plt.show()
			plt.imshow(mask2, cmap='gray'); plt.show()
			plt.imshow(mask3, cmap='gray'); plt.show()
			plt.imshow(mask4, cmap='gray'); plt.show()
			plt.imshow(cv2.cvtColor(IMAGE_EXTRACT, cv2.COLOR_BGR2RGB)); plt.show()
	return(IMAGE_EXTRACT, CONTOURS)

threshold = setWhite*0.75 #or setWhite - 10 etc... 
imgExtract, contours = __DETECT_PLANTS__(IMAGE=img, THRESHOLD=threshold, COLOR_COLLAPSE="gray", PLOT=False)
# plt.imshow(cv2.cvtColor(imgExtract, cv2.COLOR_BGR2RGB)); plt.show()

#remove pot and shadows, specifically ~gray pixels (be sure to press the soil media so that it won't be visible during phenotyping)
def __REMOVE_GRAYS__(IMAGE, EPSILON=0.10):
	for row in range(IMAGE.shape[0]):
		#print(row)
		for col in range(IMAGE.shape[1]):
			if IMAGE[row, col, 0]!=0 and IMAGE[row, col, 1]!=0 and IMAGE[row, col, 2]!=0:
				if abs(1-(float(IMAGE[row, col, 0])/IMAGE[row, col, 1]))<EPSILON and abs(1-(float(IMAGE[row, col, 0])/IMAGE[row, col, 2]))<EPSILON and abs(1-(float(IMAGE[row, col, 1])/IMAGE[row, col, 2]))<EPSILON:
					#print("REPLACE!")
					IMAGE[row, col, :] = [0, 0, 0]
	return(IMAGE)
	# plt.imshow(cv2.cvtColor(imgExtract, cv2.COLOR_BGR2RGB)); plt.show()

imgExtract2 = __REMOVE_GRAYS__(IMAGE=imgExtract, EPSILON=0.10)

#remove black pixels and flatten the array for computing the pixel values per channel (used inside the __EXTRACT_METRICS_IND__ function)
def __REMOVE_BLACK_PIXELS__(IMAGE):
	FILTERED=[]
	for row in range(IMAGE.shape[0]):
		for col in range(IMAGE.shape[1]):
			if IMAGE[row, col, 0]!=0 or IMAGE[row, col, 1]!=0 or IMAGE[row, col, 2]!=0:
				FILTERED.append((IMAGE[row,col,0], IMAGE[row,col,1], IMAGE[row,col,2]))
	return(np.array(FILTERED))

#trying to make __REMOVE_BLACK_PIXELS__(IMAGE) more efficient - faster!
# test = [trio for sub in imgExtract for trio in sub if sum(trio==[0,0,0])!=3] #still oh so very slow

#extract plant metrics per individual plant i.e. leaf/whole plant area, AUC in green channel histogram, average pixel values in BGR channels, ....
def __EXTRACT_METRICS_IND__(IMAGE, CONTOURS):
	DTYPES = [('area', int), ('blue', int), ('green', int), ('red', int), ('greeness', float)]
	FILTERED = __REMOVE_BLACK_PIXELS__(IMAGE)
	AREA = sum([cv2.contourArea(x) for x in contours])
	BLUE = np.mean(FILTERED[:,0])
	GREEN = np.mean(FILTERED[:,1])
	RED = np.mean(FILTERED[:,2])
	GREENESS = ( ((1+((GREEN-RED)/(GREEN+RED)))/2) + (1-((BLUE+GREEN+RED)/(3*255))) ) / 2
	OUT = np.array((AREA, BLUE, GREEN, RED, GREENESS), dtype=DTYPES)
	return(OUT)

OUT = __EXTRACT_METRICS_IND__(IMAGE=imgExtract2, CONTOURS=contours)


#DETECT SHADOWS - mean pixels values: B/G ~ B/R ~ G/R ~ 1.00 --> AND REMOVED THEM! [cut-off at 1-B/R > 0.10] {extras: green(0,100,0), yellow-green[0,100, 50], orangey[0,50,100], brownish[0,10,100]}
i=0
while i < OUT.shape[0]:
	test1 = 1.0 - (float(OUT['blue'][i]) / OUT['red'][i]) #grays (shadows) have blue/red ~ 1.00
	test2 = 1.0 - (float(OUT['blue'][i]) / OUT['green'][i]) #grays (shadows) have blue/green ~ 1.00
	if test1 < 0.10 and test2 < 0.10:
		# add another test to see if there is a plant within the shadowy realms!
		TEST_COOR = contours[OUT['plant'][i]]
		TEST_x,TEST_y,TEST_w,TEST_h = cv2.boundingRect(TEST_COOR)
		TEST = imgExtract[TEST_y:TEST_y+TEST_h, TEST_x:TEST_x+TEST_w]
		TEST_THRESH = setWhite - 50
		TEST_IMG, TEST_CONT = __DETECT_PLANTS__(IMAGE=TEST, THRESHOLD=TEST_THRESH, COLOR_COLLAPSE="gray", PLOT=False)
		TEST_OUT = __EXTRACT_METRICS__(IMAGE=TEST_IMG, CONTOURS=TEST_CONT, AREA_THRESHOLD=area_threshold, PLOT=False)
		imgExtract[TEST_y:TEST_y+TEST_h, TEST_x:TEST_x+TEST_w] = TEST_IMG
		j=0
		while j < TEST_OUT.shape[0]:
			test1 = 1.0 - (float(OUT['blue'][i]) / OUT['red'][i]) #grays (shadows) have blue/red ~ 1.00
			test2 = 1.0 - (float(OUT['blue'][i]) / OUT['green'][i]) #grays (shadows) have blue/green ~ 1.00
			if test1 < 0.10 and test2 < 0.10:
				TEST_OUT = np.delete(TEST_OUT, j, 0)
			else:
				j = j+1
		if TEST_OUT.shape[0] > 0:
			TEST_AREA = sum(TEST_OUT['area'])
			TEST_blue = sum(TEST_OUT['area']*TEST_OUT['blue'])/TEST_AREA
			TEST_green = sum(TEST_OUT['area']*TEST_OUT['green'])/TEST_AREA
			TEST_red = sum(TEST_OUT['area']*TEST_OUT['red'])/TEST_AREA
			TEST_greeness = sum(TEST_OUT['area']*TEST_OUT['greeness'])/TEST_AREA
			OUT['area'][i] = TEST_AREA
			OUT['blue'][i] = TEST_blue
			OUT['green'][i] = TEST_green
			OUT['red'][i] = TEST_red
			OUT['greeness'][i] = TEST_greeness
		else:
			OUT = np.delete(OUT, i, 0)
	else:
		i = i+1

#MERGE supposedly leaves from same plant [DIST=img.shape[1]/3; BINS: 0-DIST, DIST+1-2DIST, 2DIST+1-3DIST]
DIST = img.shape[1]/3
ADJ = 100 #adjusting for horizontally oriented leaves overlapping with the area of neighboring plant/s which is set as 1/3 or the image area
PLANT=[]
i=1
while i < OUT.shape[0]:
	POS = OUT['x_coor'][i]
	for j in range(i-1, -1, -1):
		START=DIST*(j)
		END=(DIST*(j+1))-ADJ
		print("----------------------")
		print("i is: " + str(i))
		print("j is: " + str(j))
		print("POS is: " + str(POS))
		print("RANGE is: " + str(START) + " - " + str(END))
		if POS >= START and POS <= END:
			OUT['area'][i-1] = OUT['area'][i-1] + OUT['area'][i]
			OUT['blue'][i-1] = ((OUT['blue'][i-1]*OUT['area'][i-1]) + (OUT['blue'][i]*OUT['area'][i])) / (OUT['area'][i-1] + OUT['area'][i])
			OUT['green'][i-1] = ((OUT['green'][i-1]*OUT['area'][i-1]) + (OUT['green'][i]*OUT['area'][i])) / (OUT['area'][i-1] + OUT['area'][i])
			OUT['red'][i-1] = ((OUT['red'][i-1]*OUT['area'][i-1]) + (OUT['red'][i]*OUT['area'][i])) / (OUT['area'][i-1] + OUT['area'][i])
			OUT = np.delete(OUT, i, 0)
			print("Delete!!!")
		elif j==0:
			PLANT.append(i)
			i = i + 1
			print("Next!!!")
		print(PLANT)

#write proper plant ID names based on the filename
PLANT_ID=[]
for i in PLANT:
	if i <= 2:
		names = filename.replace(".","-").split("-")
		PLANT_ID.append(names[0] + "-" + names[i+1] + "-" + names[4])
	else:
		PLANT_ID.append("ROUGE_CONTOUR")

OUT = OUT.astype([('x_coor', int), ('y_coor', int), ('plant', np.str_, 16),('area', int), ('blue', int), ('green', int), ('red', int), ('greeness', float)])
OUT['plant'] = PLANT_ID

# write out the greeness metrics and the mask-extracted leaf segment image with blacked out rogue contours
cv2.imwrite('COLORCORRECT-' + filename.split(".",1)[0] + '.jpg', img)
cv2.imwrite('EXTRACT-' + filename.split(".",1)[0] + '.jpg', imgExtract)
np.savetxt(fname='OUT-' + filename.split(".",1)[0] + '.csv', X=OUT, delimiter=',', fmt=['%i', '%i', '%s', '%i', '%i', '%i', '%i', '%.2f'], header='x_coor, y_coor, area (px), greeness_non-parametric')

# #run
# ls INPUT/ | grep '.JPG' > IMAGES.list
# chmod +x test_detectPlants2.py
# nohup parallel ./test_detectPlants2.py INPUT/{} ::: $(cat IMAGES.list) &
# mv EXTRACT* OUTPUT/IMAGES/
# mv COLORCORRECT* OUTPUT/IMAGES/
# mv OUT* OUTPUT/CSV/

# #debugging some more
# for i in $(ls INPUT/ | grep TRAY04)
# do
# echo $i
# ./test_detectPlants2.py INPUT/${i}
# done