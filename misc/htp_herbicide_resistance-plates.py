#!/usr/bin/env python
import os, sys
import cv2
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.integrate import simps
import scipy.stats as stats
from scipy.stats import norm
from scipy.optimize import minimize
import sys

#import the image
#filename="p3-d6.jpg"
#filename="DSC_0010.JPG"
filename = sys.argv[1]
img = cv2.imread(filename)
nrows, ncols, nchannels = img.shape

#crop
img = img[600:2400,:]

#white balancing using top-left positioned 255,255,255 white card
# mean_column = np.uint8(np.mean(img[0:200,0:200], axis=0))
# mean_row = np.uint8(np.mean(mean_column, axis=0))
# # I just set the white card as a gray card to compensate for the shadow below the plates and 
# # since I cannot actually get a card that is completely white (255,255,255) because 
# # that will mean that the card is actually emmitting light, right????!!!
# diff = cv2.subtract(np.array([[[200,200,200]]], np.uint8), np.reshape(mean_row, (1,1,3)))
# white_correction = np.full((nrows, ncols, nchannels), diff, np.uint8)
# img = cv2.add(img, white_correction)
# CORRECTION FACTOR: 255/mean
SET_WHITE = 140
mean_column = np.mean(img[0:200,0:200], axis=0) #140:300, 2:220
mean_row = np.mean(mean_column, axis=0)
CORRECTION_FACTOR = SET_WHITE/mean_row #setting the white styrofoam box color to 140-140-140
img = np.uint8(CORRECTION_FACTOR*img)
plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB)); plt.show()

#extract the blue channel
# blue = img[:,:,0]
#plt.imshow(blue, cmap='gray'); plt.show()

#let's try gray scaling instead of not capturing the red channel as above - the blue one
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
plt.imshow(gray, cmap='gray'); plt.show()
blue = gray

#detect the leaf segments
thresh, mask_blue = cv2.threshold(blue, SET_WHITE-10, 255, cv2.THRESH_BINARY_INV)
plt.imshow(mask_blue, cmap='gray'); plt.show()

#dilation and constriction of pixels to maximize leaf area detection and minimize noise
kernel = np.ones((5,5),np.uint8)
maskCleanup1 = cv2.morphologyEx(mask_blue, cv2.MORPH_OPEN, kernel)
plt.imshow(maskCleanup1, cmap='gray'); plt.show()
maskCleanup2 = cv2.morphologyEx(maskCleanup1, cv2.MORPH_CLOSE, kernel)
plt.imshow(maskCleanup2, cmap='gray'); plt.show()

#detect the contours or leaf segment objects for individual leaf separation in the next steps
MASK, contours, hierarchy = cv2.findContours(maskCleanup2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
plt.imshow(MASK, cmap='gray'); plt.show()

#extract the leaf segments from the BRG image using the contours from the previous step
imgExtract = cv2.bitwise_and(img, img, mask=MASK)
plt.imshow(cv2.cvtColor(imgExtract, cv2.COLOR_BGR2RGB)); plt.show()

#for each contour, test if the area is larger than the expected leaf area (LEAF_DETECTION_THRESHOLD)
#	- if it is calculate the greeness metrics
LEAF_DETECTION_THRESHOLD = 10000 #pixels
DTYPES = [('x_coor', int), ('y_coor', int), ('area', int), ('greeness', float), ('integral', float), ('gamma_blue', float), ('gamma_green', float), ('gamma_red', float)]
VALUES = []
for i in range(len(contours)):
	coordinates = contours[i]
	x,y,w,h = cv2.boundingRect(coordinates)
	#area = w*h
	area = cv2.contourArea(contours[i])
	if area >= LEAF_DETECTION_THRESHOLD:
		leaf = imgExtract[y:y+h, x:x+w]
		#GREENESS METRICS: (1) AUC for 100-150 interval; (2) parameter estimation using mixed normal distribution; (3) ( G/R + (1-(B+G+R/2*255)) ) / 2 
		#(1) AUC on 100-150 green channel interval
		hist = cv2.calcHist([leaf],[1],None,[256],[1,256])
		hist_flat = [j for sub in hist for j in sub]
		INTEGRAL = simps(hist_flat[100:150], range(100,150)) # healthy green range set from 100 - 150 in the green channel based on a few samples
		#(2) mixture paramter estimation of bimodal distribution (mixure of 2 normal distributions)
		for channel in range(0, 3):
			px = [k for sub in leaf[:,:,channel] for k in sub]
			filtered = filter(lambda a:a!=0, px) #remove black pixels
			DATA = pd.DataFrame(filtered, columns=['data'])
			density = stats.kde.gaussian_kde(DATA.values.ravel())
			domain = np.linspace(0, 255, 255)
			density = density(domain)
			DATA = density
			def mixture_distn(param):
				p, s1, s2 = param
				ESTIMATE = (p*norm.pdf(range(1,256), loc=u1, scale=s1)) + ((1-p)*norm.pdf(range(1,256), loc=u2, scale=s2))
				return sum((DATA-ESTIMATE)**2)
			initial_param = [0.05,0.01,0.01]
			param_bounds = ((0,1), (1,1000), (1,255))
			if channel == 0:
				u1, u2 = 35, 50
				GAMMA_blue = minimize(mixture_distn, x0=initial_param, bounds=param_bounds)
			elif channel == 1:
				u1, u2 = 120, 160
				GAMMA_green = minimize(mixture_distn, x0=initial_param, bounds=param_bounds)
			else:
				u1, u2 = 120, 160
				GAMMA_red = minimize(mixture_distn, x0=initial_param, bounds=param_bounds)
		#(3) non-parametric formula E( G/R + lightness )
		blueChannel = leaf[:, :, 0]; blueMean = np.mean(blueChannel[np.nonzero(blueChannel)])
		greenChannel = leaf[:, :, 1]; greenMean = np.mean(greenChannel[np.nonzero(greenChannel)])
		redChannel = leaf[:, :, 2]; redMean = np.mean(redChannel[np.nonzero(redChannel)])
		GREENESS = ( ((1+((greenMean-redMean)/(greenMean+redMean)))/2) + (1-((blueMean+greenMean+redMean)/(3*255))) ) / 2
		VALUES.append((x, y, area, GREENESS, INTEGRAL, GAMMA_blue.x[0], GAMMA_green.x[0], GAMMA_red.x[0]))
		print(i)
		# print(x)
		# print(y)
		print(area)
		plt.imshow(cv2.cvtColor(leaf, cv2.COLOR_BGR2RGB)); plt.show()
		# plt.plot(hist); plt.show()
		plt.plot(density); plt.show()
		# print(GREENESS)
		# print(INTEGRAL)
		# #print(GAMMA.x[0])
	# else:
	# 	imgExtract[y:y+h, x:x+w] = np.zeros(shape=(h,w,3))

#plt.imshow(cv2.cvtColor(imgExtract, cv2.COLOR_BGR2RGB)); plt.show()
OUT = np.array(VALUES, dtype=DTYPES)
OUT = np.sort(OUT, order=['x_coor', 'y_coor'])
# if(OUT[0,][0]==0):
# 	OUT = OUT[1:OUT.shape[0],] # remove the black color correction card

# grouping the data by column in the plates
#	(if the difference in the positions of two leaf segments in the x-axis is 
#	less than the standard error between all leaf positions in the x-axis then they belong in the same column)
# GROUP = min(OUT['x_coor'])
# STDER = np.sqrt(np.var(OUT['x_coor']))/np.sqrt(len(OUT['x_coor']))
# COLUMN_GROUP = []
# for i in OUT['x_coor']:
# 	if abs(GROUP-i) < STDER:
# 		COLUMN_GROUP.append(GROUP)
# 	else:
# 		GROUP = i
# 		COLUMN_GROUP.append(GROUP)

# OUT['x_coor'] = COLUMN_GROUP
# OUT = np.sort(OUT, order=['x_coor', 'y_coor'])
OUT = np.sort(OUT, order=['x_coor'])

# write out the greeness metrics and the mask-extracted leaf segment image with blacked out rogue contours
np.savetxt(fname=filename.split(".",1)[0] + '-out.csv', X=OUT, delimiter=',', fmt=['%i', '%i', '%i', '%.2f', '%.2f', '%.2f', '%.2f', '%.2f'], header='x_coor, y_coor, area (px), greeness_non-parametric, greeness_AUC_100-150interval, mixture_param_blue, mixture_param_green, mixture_param_red')
cv2.imwrite(filename.split(".",1)[0] + '-out.jpg', imgExtract)


# ################### MISC
# #colors in BRG color system
# blue = np.full(shape=(1000,1000,3), fill_value=[255,0,0], dtype=np.uint8)
# plt.imshow(cv2.cvtColor(blue, cv2.COLOR_BGR2RGB)); plt.show()

# green = np.full(shape=(1000,1000,3), fill_value=[0,255,0], dtype=np.uint8)
# plt.imshow(cv2.cvtColor(green, cv2.COLOR_BGR2RGB)); plt.show()

# red = np.full(shape=(1000,1000,3), fill_value=[0,0,255], dtype=np.uint8)
# plt.imshow(cv2.cvtColor(red, cv2.COLOR_BGR2RGB)); plt.show()

# white = np.full(shape=(1000,1000,3), fill_value=[255,255,255], dtype=np.uint8)
# plt.imshow(cv2.cvtColor(white, cv2.COLOR_BGR2RGB)); plt.show()

# black = np.full(shape=(1000,1000,3), fill_value=[0,0,0], dtype=np.uint8)
# plt.imshow(cv2.cvtColor(black, cv2.COLOR_BGR2RGB)); plt.show()

# x = np.full(shape=(1000,1000,3), fill_value=[100,100,100], dtype=np.uint8)
# plt.imshow(cv2.cvtColor(x, cv2.COLOR_BGR2RGB)); plt.show()

# cyan = cv2.add(blue, green)
# plt.imshow(cv2.cvtColor(cyan, cv2.COLOR_BGR2RGB)); plt.show()

# magenta = cv2.add(blue, red)
# plt.imshow(cv2.cvtColor(magenta, cv2.COLOR_BGR2RGB)); plt.show()

# yellow = cv2.add(green, red)
# plt.imshow(cv2.cvtColor(yellow, cv2.COLOR_BGR2RGB)); plt.show()

