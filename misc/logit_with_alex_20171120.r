#making sense of the output of HTP on sulfometuron test 2017-11-13 to 2017-11-20
# (1) Determine response variable the maximizes the variance explained by herbicide dose and genotype effects
# (2) build the model using that best response variable
# (3) extract the predicted response variable
# (4) the quantitative measure of resistance is ....

library(lme4)

dat = read.csv("htp_col_input.csv", T)
#dat = read.csv("htp_rand_resist.csv", T); dat$Resistance = dat$ResistanceRAND12
#dat = subset(dat, Herbicide!=1); dat = subset(dat, Herbicide!=4); dat = subset(dat, Herbicide!=16); dat = subset(dat, Herbicide!=64); dat = droplevels(dat)

#find the response variable that maximizes the variance explained by the herbicide dose and genotype effects!
varpart = matrix(NA, ncol=3, nrow=0)
for (i in names(dat)[grepl("_",names(dat))]) {
print(i)
VCV=as.data.frame(VarCorr(lmer(get(i)~1+(log1p(Herbicide)|Plant),data=dat)))
varpart=rbind(varpart,VCV[is.na(VCV[,3]),4])
}
rownames(varpart)=names(dat)[grepl("_",names(dat))]
herit = apply(varpart,1,function(x) x[2]/sum(x))
i = names(herit)[herit==max(herit)]

#using the selected response variable i:
#build the model accounting for both herbicide dose and genotype effects; and
#extract predicted response, Y_hat.
model_1 = lmer(get(i)~1+(log1p(Herbicide)|Plant),data=dat)
Y_hat = (getME(model_1,'X')%*%fixef(model_1)) + (getME(model_1,'Z')%*%unlist(ranef(model_1)))
dat$Y_hat = Y_hat[,1]

#check the fit between the predicted response variable with the empirical resistance data
#generate random resistance values for checking
#dummy_resistance = sample(c(0,1), replace=T, size=nrow(dat)/2)
#dat$Resistance = rep(dummy_resistance, each=2)
logmodel_1 = glm(Resistance~Y_hat, family=binomial, data=dat)

plot(Resistance~Y_hat,data=dat)
curve(predict(logmodel_1,data.frame(Y_hat=x),type="resp"),add=TRUE,col=2)
points(Y_hat,fitted(logmodel_1),pch=20,col=2)

diff = dat$Resistance - fitted(logmodel_1)
wrongers = sum(abs(diff)>0.50)

#which plants are resistance and susceptible?
quant.dat = data.frame(Y_hat[,1], dat$Plant)
colnames(quant.dat) = c("Y_hat", "Plant")
boxplot(quant.dat$Y_hat~quant.dat$Plant)
boxplot(dat$Resistance~quant.dat$Plant)
MEANS = aggregate(Y_hat~Plant, FUN=mean, data=quant.dat)
View(MEANS)








###############################################################
###############################################################
###############################################################







#picking pairs of dosages that minimizes misclassification!

library(lme4)

DAT = read.csv("htp_col_input.csv", T)

MISCLASSIFIED = matrix(NA, ncol=1, nrow=0)
pair.names = c()
herit.optim = c()
metric.names = c()
for(dose1 in levels(as.factor(DAT$Herbicide))) {
	for(dose2 in levels(as.factor(DAT$Herbicide))[levels(as.factor(DAT$Herbicide))!=dose1]) {
		pair.names = c(pair.names, paste(dose1, dose2, sep="x"))

		DAT = read.csv("htp_col_input.csv", T)
		dat = subset(DAT, Herbicide==dose1 | Herbicide==dose2) ; dat = droplevels(dat)

		#find the response variable that maximizes the variance explained by the herbicide dose and genotype effects!
		varpart = matrix(NA, ncol=3, nrow=0)
		for (i in names(dat)[grepl("_",names(dat))]) {
			print(i)
			VCV=as.data.frame(VarCorr(lmer(get(i)~1+(log1p(as.numeric(Herbicide))|Plant),data=dat)))
			varpart=rbind(varpart,VCV[is.na(VCV[,3]),4])
		}
		rownames(varpart)=names(dat)[grepl("_",names(dat))]
		herit = apply(varpart,1,function(x) x[2]/sum(x))
		i = names(herit)[herit==max(herit)]
		herit.optim = c(herit.optim, max(herit))
		metric.names = c(metric.names, i)

		#using the selected response variable i:
		#build the model accounting for both herbicide dose and genotype effects; and
		#extract predicted response, Y_hat.
		model_1 = lmer(get(i)~1+(log1p(as.numeric(Herbicide))|Plant),data=dat)
		Y_hat = (getME(model_1,'X')%*%fixef(model_1)) + (getME(model_1,'Z')%*%unlist(ranef(model_1)))
		dat$Y_hat = Y_hat[,1]

		#check the fit between the predicted response variable with the empirical resistance data
		#generate random resistance values for checking
			#dummy_resistance = sample(c(0,1), replace=T, size=nrow(dat)/2)
			#dat$Resistance = rep(dummy_resistance, each=2)
		logmodel_1 = glm(Resistance~Y_hat, family=binomial, data=dat)

		plot(Resistance~Y_hat,data=dat)
		curve(predict(logmodel_1,data.frame(Y_hat=x),type="resp"),add=TRUE,col=2)
		points(Y_hat,fitted(logmodel_1),pch=20,col=2)

		diff = dat$Resistance - fitted(logmodel_1)
		wrongers = sum(abs(diff)>0.50)
		MISCLASSIFIED = rbind(MISCLASSIFIED, wrongers)
		#cor(dat$Resistance, fitted(logmodel_1), method='pearson')
	}
}
MISCLASSIFIED = data.frame(pair.names, metric.names, herit.optim, MISCLASSIFIED[,1])
colnames(MISCLASSIFIED) = c("Dose_Pair", "Optim_Metric", "Max_herit", "Misclassified_Count")
View(MISCLASSIFIED)






###############################################################
###############################################################
###############################################################






### iterating across dosage

library(lme4)
library(agricolae)

dat = read.csv("htp_col_input.csv", T)

HERITABILITIES = matrix(NA, ncol=0, nrow=nlevels(as.factor(dat$Herbicide)))
for (i in names(dat)[grepl("_",names(dat))]) {
	print(i)
	HERIT = matrix(NA, ncol=1, nrow=0)
	for(dose in levels(as.factor(dat$Herbicide))) {
		print(dose)
		data = subset(dat, Herbicide==dose); data = droplevels(data)
		model = lmer(get(i) ~ 1 + 1|Plant, data)
		vcv = as.data.frame(VarCorr(model))
		herit = vcv$vcov[1]/sum(vcv$vcov)
		HERIT = rbind(HERIT, herit)
	}
	HERITABILITIES = cbind(HERITABILITIES, HERIT)
}
rownames(HERITABILITIES) = levels(as.factor(dat$Herbicide))
colnames(HERITABILITIES) = names(dat)[grepl("_",names(dat))]

DATA = subset(dat, Herbicide==0)
#DATA=dat
model_lmer = lmer(Metric1_GPD ~ 1 + Time|Plant, DATA)
model_lm = lm(Metric1_GPD ~ 1 + Time*Plant, DATA)

HSD = HSD.test(model_lm, trt="Plant")
View(HSD$groups)

# Y_hat = (getME(model_lmer,'X')%*%fixef(model_lmer)) + (getME(model_lmer,'Z')%*%unlist(ranef(model_lmer)))
# DATA$Y_hat = Y_hat[,1]

# logmodel_1 = glm(Resistance~Y_hat, family=binomial, data=DATA)

# plot(Resistance~Y_hat,data=DATA)
# curve(predict(logmodel_1,data.frame(Y_hat=x),type="resp"),add=TRUE,col=2)
# points(Y_hat,fitted(logmodel_1),pch=20,col=2)






###############################################################
###############################################################
###############################################################



# using AUC across time

library(MESS)
library(agricolae)

dat = read.csv("htp_row_input.csv", T)
#dat = subset(dat, Time!="D0"); dat = droplevels(dat)

metric="Metric1_GPD"
#metric="Metric2_AUC"
#metric="Metric3_MXP"
names.DOSE = c(); names.PLANT = c(); names.REP = c()
output.AUC = c()
for(dose in levels(as.factor(dat$Herbicide))) {
	for(plant in levels(dat$Plant)) {
		for(rep in levels(dat$Rep)) {
			DAT = subset(dat, Herbicide==dose); DAT = droplevels(DAT)
			DAT_LR = subset(DAT, Plant==plant); DAT_LR = droplevels(DAT_LR)
			DAT_LR_r = subset(DAT_LR, Rep==rep); DAT_LR_r = droplevels(DAT_LR_r)
			x = 0:(nrow(DAT_LR_r)-1)
			y = eval(parse(text=paste("DAT_LR_r$", metric, sep="")))
			plot(x, y)
			lines(x[order(x)], y[order(x)], xlim=range(x), ylim=range(y), pch=16)
			AUC = auc(x, y, from = min(x), to = max(x), type = c("linear", "spline"), absolutearea = FALSE)
			output.AUC = c(output.AUC, AUC)
			names.DOSE = c(names.DOSE, dose); names.PLANT = c(names.PLANT, plant); names.REP = c(names.REP, rep)
		}
	}
}

OUT = data.frame(names.REP, names.DOSE, names.PLANT, output.AUC)
colnames(OUT) = c("REP", "DOSE", "PLANT", "GPD_AUC")

model = lm(GPD_AUC ~ PLANT, OUT)
anova(model)
HSD = HSD.test(model, trt="PLANT")











###########################################################
###########################################################
###########################################################
###########################################################
###########################################################





#which responce variable (daily GPD, AUC and MXP for 7 days) minimizes misclassification

library(lme4)

dat = read.csv("htp_col_input.csv", T)
#include only dose: 0, 2, 8 & 32 g ai/L
dat = subset(dat, Herbicide!=0); dat = subset(dat, Herbicide!=2); dat = subset(dat, Herbicide!=8); dat = subset(dat, Herbicide!=32); dat = droplevels(dat)

#find the response variable that maximizes the variance explained by the herbicide dose and genotype effects!
response_var = c(); h2 = c(); wrongers = c()
for (i in names(dat)[grepl("_",names(dat))]) {
	model = lmer(get(i)~1+(log1p(Herbicide)|Plant),data=dat)
	Y_hat = (getME(model,'X')%*%fixef(model)) + (getME(model,'Z')%*%unlist(ranef(model)))
	dat$Y_hat = Y_hat[,1]

	logmodel = glm(Resistance~Y_hat, family=binomial, data=dat)

	plot(Resistance~Y_hat,data=dat)
	curve(predict(logmodel,data.frame(Y_hat=x),type="resp"),add=TRUE,col=2)
	points(Y_hat,fitted(logmodel),pch=20,col=2)
	diff = dat$Resistance - fitted(logmodel)
	#diff = dat$Resistance - Y_hat
		
	#tabulating everything
	response_var = c(response_var, i)
	h2 = c(h2, VarCorr(model)$Plant[1,1] / (VarCorr(model)$Plant[1,1] + VarCorr(model)$Plant[2,2] + attr(VarCorr(model), 'sc')^2))
	wrongers = c(wrongers, sum(abs(diff)>0.50))

	#which plants are resistance and susceptible?
	quant.dat = data.frame(Y_hat[,1], dat$Plant)
	colnames(quant.dat) = c("Y_hat", "Plant")
	#boxplot(quant.dat$Y_hat~quant.dat$Plant)
	MEANS = aggregate(Y_hat~Plant, FUN=mean, data=quant.dat)
	#View(MEANS)
}
out = data.frame(response_var, h2, wrongers)
write.csv(out, "logit-analysis-out.csv")

#ok so now it seems 4 days and using just 4 dosages are enough:
library(agricolae)
model = lm(D4_GPD ~ Herbicide+Plant, dat)
anova(model)
HSD = HSD.test(model, trt="Plant")
HSD
HSD = HSD.test(model, trt="Herbicide")
HSD








###############################################################
###############################################################
###############################################################






#iterating across number of doses, doses, day and metric

library(lme4)

dat = read.csv("htp_col_input.csv", T)

ndoses = nlevels(as.factor(dat$Herbicide))
doses = as.numeric(levels(as.factor(dat$Herbicide)))
nmetrics = length(names(dat)[grep("_", names(dat))])
metrics = names(dat)[grep("_", names(dat))]

for (i in 1:ndoses) {
	#dose combinations = n!/(n-i)!i! [e.g. i=2 --> 8*7/2 = 28]
	for (j in )
}