#!/bin/bash

#rename pictures (for trios)

echo -e "A1-B1-C1" > MASTER.list
echo -e "D1-E1-F1" >> MASTER.list
echo -e "A2-B2-C2" >> MASTER.list
echo -e "D2-E2-F2" >> MASTER.list
echo -e "A3-B3-C3" >> MASTER.list
echo -e "D3-E3-F3" >> MASTER.list
echo -e "A4-B4-C4" >> MASTER.list
echo -e "D4-E4-F4" >> MASTER.list
echo -e "A5-B5-C5" >> MASTER.list
echo -e "D5-E5-F5" >> MASTER.list
echo -e "A6-B6-C6" >> MASTER.list
echo -e "D6-E6-F6" >> MASTER.list
echo -e "A7-B7-C7" >> MASTER.list
echo -e "D7-E7-F7" >> MASTER.list

ls | grep '10*D*' > DCIM_folders.list

TRAY=1
PICS=1
TRIO=1
REP=1
for i in $(cat DCIM_folders.list)
do 
cd $i
ls | grep '.JPG' > IMAGE_files.list
for j in $(seq 1 $(wc -l IMAGE_files.list | cut -d' ' -f1))
do
file=$(head -n$j IMAGE_files.list | tail -n1)
coor=$(head -n$TRIO ../MASTER.list | tail -n1)
#rename
if [ $TRAY -lt 10 ]; then tray=$(echo 0${TRAY}); else tray=$TRAY; fi
if [ $REP -lt 10 ]; then rep=$(echo 0${REP}); else rep=$REP; fi
if [ $REP -lt 5 ]; then mv $file TRAY${tray}-${coor}-REP${rep}.JPG; else mv $file TRAY${tray}-000.JPG; fi
#update variables
if [ $REP -lt 5 ]; then REP=$(echo ${REP}+1 | bc); else REP=1; fi
if [ $REP -eq 5 ] && [ $PICS -lt 70 ]; then TRIO=$(echo ${TRIO}+1 | bc); elif [ $PICS -eq 70 ]; then TRIO=1; fi
if [ $PICS -lt 70 ]; then PICS=$(echo ${PICS}+1 | bc); else PICS=1; fi
if [ $PICS -eq 70 ]; then TRAY=$(echo ${TRAY}+1 | bc); fi
done
cd ..
done

rm *.list 10*/*.list
mv 10*/*.JPG .
rm -R 10*/

############################################################################################# 20180213

# rename pictures of individual plants (4 reps or 4 POV)

#1: generate filenames list
touch filenames.list
for i in $(seq 1 24)
do
for j in $(seq 1 7)
do
for k in A B C D E F
do
for l in $(seq 1 4)
do
if [ $i -lt 10 ]
then
echo -e "TRAY0$i-$k$j-REP$l.jpg" >> filenames.list
else
echo -e "TRAY$i-$k$j-REP$l.jpg" >> filenames.list
fi
done
done
done
done

#2: remove marker pictures manually while checking for duplications or ommisions during photographing

#3: list all JPG files we will rename
find | grep .JPG | sort > images.list

#4: check if we have the same number of expected pictures (filenames.list) and the available pictures (images.list)
wc -l filenames.list
wc -l images.list

#5: rename!
mkdir INPUT

for image in $(seq 1 $(wc -l filenames.list | cut -d' ' -f1))
do
oldName=$(head -n$image images.list | tail -n1)
newName=$(head -n$image filenames.list | tail -n1)
cp $oldName INPUT/${newName}
echo $image
done

mkdir RAW_IMAGES
mv 10*D*/ RAW_IMAGES
rm images.list