#!/usr/bin/env Rscript
#args = commandArgs(trailingOnly=TRUE)

########################################################################
###################
###				###
### BINARY DATA ###
###				###
###################
#input=args[1]
work_dir="/home/student.unimelb.edu.au/jparil/Documents/PHENOMICS - herbicide resistance leaf assays/instaGraminoid/misc/EXP16_OUTPUT/ANALYSIS"
data_filename="INPUT/EXP16_ANALYSIS_INPUT.csv"

setwd(work_dir)
dat = read.csv(data_filename)

#use only counts at day 14 rep1 and remove empty pots
d = subset(subset(subset(dat, DAY=="DAY14"), REPLI=="REP1"), AREA>0)
d = droplevels(d)


#chi^2 test for independence
CHISQ = c()
for(h in levels(d$HERBI)){
	DATA = subset(d, HERBI==h)
	TABLE = table(DATA$POPUL, DATA$SURVI)
	CHISQ = c(CHISQ, 
		tryCatch(
		{chisq.test(DATA$POPUL, DATA$SURVI)$p.value},
		error = function(e) {NA})
		)
}
CHISQ = data.frame(levels(d$HERBI), CHISQ)
colnames(CHISQ) = c("HERBI", "p.value")

#resistance table
P.R = aggregate(SURVI~POPUL + HERBI, data=d, FUN=mean)
P.R = P.R[order(P.R$POPUL),]
P.R = P.R[order(P.R$HERBI),]

#merge resistance proportion and chi^2 test p-values
OUT1= data.frame(unique(P.R$HERBI))
OUT1 = as.data.frame(cbind(OUT1, matrix(P.R$SURVI, ncol=2, byrow=T)))
colnames(OUT1) = c("HERBI", levels(P.R$POPUL))
OUT1 = merge(OUT1, CHISQ, by="HERBI")

#write out
write.csv(OUT1, file="OUTPUT/BINARY_DATA-ChisquareTest.csv")
########################################################################

########################################################################
########################################
###									 ###
### instaGraminoid QUANTITATIVE DATA ###
###									 ###
########################################
library(glmnet)
library(ggplot2)

work_dir="/home/student.unimelb.edu.au/jparil/Documents/PHENOMICS - herbicide resistance leaf assays/instaGraminoid/MISC/EXP16_OUTPUT/ANALYSIS"
quantitative_data_filename = "OUTPUT/CV_DATA.csv"	#output of "cross_validation.r"

setwd(work_dir)
DATA = read.csv(quantitative_data_filename)

# extracting the quantitative herbicide resistance estimates from multivariate logistic model predicted values
metrics=colnames(DATA)[6:ncol(DATA)]
#metrics = metrics[!grepl("DAY", metrics)]	#removed individual day measurements because they're highly correlated with the DELTA's which seem to be better as we're mneasuring the changes in the metric through time
X = eval(parse(text=paste("cbind(", paste(paste("DATA$", metrics, sep=""), collapse=","), ")", sep="")))
colnames(X) = metrics
model = glmnet(x=X, y=as.factor(DATA$SURVI), family="binomial", alpha=0.5, nlambda=100) #alpha=0:RR penalty; alpha=1:Lasso penalty
lambdai = model$dev.ratio==max(model$dev.ratio)
QUANT.RES = model$a0[lambdai] + (X%*%model$beta[,lambdai])
pred = 1/(1+exp(-QUANT.RES))
b = as.data.frame(model$beta[,lambdai])
m = rownames(b)
beta = data.frame(m,b); rownames(beta) = 1:nrow(beta); colnames(beta)=c("metrics", "beta")
beta = beta[order(abs(beta$beta), decreasing=TRUE), ]
class = ifelse(pred > 0.50, 1, 0)
right = sum(class==DATA$SURVI)
wrong = sum(class!=DATA$SURVI)
perfo = right/sum(right, wrong)
DATA$QUANT.RES = scale(QUANT.RES)

# t-test of populations per herbicide
T = c(); M = c()
for(h in levels(DATA$HERBI)){
	df = subset(DATA, HERBI==h)
	df=droplevels(df)
	T = c(T, 
		tryCatch(
		{t.test(df$QUANT.RES~df$POPUL)$p.value},
		error = function(e) {NA})
		)
	M = c(M, max(df$QUANT.RES))

}
T = data.frame(levels(DATA$HERBI), T, M)
colnames(T) = c("HERBI", "p.value", "max")
sig = c()
for(i in T$p.value) {if(i<0.001) {sig=c(sig, "***")} else if(i<0.01) {sig=c(sig, "**")} else if(i<0.05) {sig=c(sig, "*")} else {sig=c(sig, "ns")}}
T$sig = sig
write.csv(T, file="OUTPUT/QUANTI_DATA-tTest.csv")


#violon plots
dT = rbind(T,T)
dT$POPULATION = rep(levels(DATA$POPUL), each=nrow(T)/2)

DATA$POPULATION = DATA$POPUL
dodge <- position_dodge(width = 0.4)
dp <- ggplot(DATA, aes(x=HERBI, y=QUANT.RES, fill=POPULATION)) + 
  geom_violin(trim=FALSE, position=dodge)+
  geom_boxplot(width=0.1, position=dodge)+
  labs(x="HERBICIDE", y = "RESISTANCE ESTIMATE")+
  geom_text(data=dT, aes(x=HERBI, y=max, label=sig), col="black", size=7)+
  theme(text=element_text(size=20), axis.text=element_text(size=20), legend.position=c(0.9, 0.1))

jpeg("OUTPUT/instaGraminoid_violin_quantResistance.jpeg", quality=100, width=1200, height=800)
dp
dev.off()
########################################################################

########################################################################
###################################
###								###
### PATTERN OF CROSS-RESISTANCE ###
###								###
###################################
library(glmnet)
library(Hmisc)
# # library(devtools)
# # install_github("vqv/ggbiplot") # to install ggbiplot for PCA pretty biplot
# library(ggbiplot)
library(ggplot2)
library(sem)

work_dir="/home/student.unimelb.edu.au/jparil/Documents/PHENOMICS - herbicide resistance leaf assays/instaGraminoid/misc/EXP16_OUTPUT/ANALYSIS"
quantitative_data_filename = "OUTPUT/CV_DATA.csv"	#output of "cross_validation.r"

setwd(work_dir)
DATA = read.csv(quantitative_data_filename)

# extracting the quantitative herbicide resistance estimates from multivariate logistic model predicted values
metrics=colnames(DATA)[6:ncol(DATA)]
#metrics = metrics[!grepl("DAY", metrics)]	#removed individual day measurements because they're highly correlated with the DELTA's which seem to be better as we're mneasuring the changes in the metric through time
X = eval(parse(text=paste("cbind(", paste(paste("DATA$", metrics, sep=""), collapse=","), ")", sep="")))
colnames(X) = metrics
model = glmnet(x=X, y=as.factor(DATA$SURVI), family="binomial", alpha=0.5, nlambda=100) #alpha=0:RR penalty; alpha=1:Lasso penalty
lambdai = model$dev.ratio==max(model$dev.ratio)
QUANT.RES = model$a0[lambdai] + (X%*%model$beta[,lambdai])
pred = 1/(1+exp(-QUANT.RES))
b = as.data.frame(model$beta[,lambdai])
m = rownames(b)
beta = data.frame(m,b); rownames(beta) = 1:nrow(beta); colnames(beta)=c("metrics", "beta")
beta = beta[order(abs(beta$beta), decreasing=TRUE), ]
class = ifelse(pred > 0.50, 1, 0)
right = sum(class==DATA$SURVI)
wrong = sum(class!=DATA$SURVI)
perfo = right/sum(right, wrong)
DATA$QUANT.RES = scale(QUANT.RES)

# generating matrices for correlation computation per pop (herb along columns & plants along rows)
genMAT <- function(DATA, METRIC) {
	for(h in levels(DATA$HERBI)){
		df = subset(DATA, HERBI==h)
		eval(parse(text=paste("ag = aggregate(", METRIC, " ~ POPUL + PLANT, df, FUN=mean)", sep="")))
		MERGE = tryCatch({merge(MERGE, ag, by=c("POPUL", "PLANT"))}, error=function(e){MERGE=ag})
	}
	colnames(MERGE) = c("POPUL", "PLANT", levels(DATA$HERBI))
	return(MERGE)
}

CORR.BIN = genMAT(DATA=DATA, METRIC="SURVI")
CORR.QUAN = genMAT(DATA=DATA, METRIC="QUANT.RES")

CORR.BIN.i = genMAT(DATA=subset(DATA, POPUL=="INVERLEIGH"), METRIC="SURVI")
CORR.QUAN.i = genMAT(DATA=subset(DATA, POPUL=="INVERLEIGH"), METRIC="QUANT.RES")

CORR.BIN.u = genMAT(DATA=subset(DATA, POPUL=="URANA"), METRIC="SURVI")
CORR.QUAN.u = genMAT(DATA=subset(DATA, POPUL=="URANA"), METRIC="QUANT.RES")

#correlation
MERGED = ls()[grep("CORR", ls())]
for (m in MERGED){
	print(m)
	CORR = rcorr(as.matrix(eval(parse(text=m))[3:6]))
	OUT = rbind(CORR$r, CORR$P)
	write.csv(OUT, file=paste("OUTPUT/", m, ".csv", sep=""))
}


#PCA
PCA.MERGE = merge(CORR.QUAN, CORR.BIN, by=c("POPUL", "PLANT"))
colnames(PCA.MERGE) = c("POPUL", "PLANT", levels(DATA$HERBI), paste("B.", levels(DATA$HERBI), sep=""))
rownames(PCA.MERGE) = PCA.MERGE$PLANT

	#add resistance groupings
lab = matrix(rep(levels(DATA$HERBI), each=nrow(PCA.MERGE)), nrow=nrow(PCA.MERGE), byrow=FALSE)
RESISTANCE = c()
for (r in 1:nrow(lab)){
	RESISTANCE = c(RESISTANCE, paste(lab[r,as.logical(as.matrix(PCA.MERGE[r,7:10]))], collapse="+"))
}

	# PRCOMP = prcomp(as.matrix(PCA.MERGE[,3:10]), center=TRUE, scale=FALSE)	#quanti + binary
PRCOMP = prcomp(as.matrix(PCA.MERGE[,3:6]), center=TRUE, scale=FALSE)	#quanti data alone!
biplot(PRCOMP) #make these plot prettier!!!20180510

PC1 = PRCOMP$x[,1]; PC2 = PRCOMP$x[,2]	#Rotated data of the first 2 PC
V = PRCOMP$sdev^2	#variance of each PC
pV.PC1 = V[1]/sum(V); pV.PC2 = V[2]/sum(V)	#proportion of variance explained by the first 2 PC

bp <- ggplot(data=data.frame(PC1, PC2), aes(PC1, PC2))+
		geom_point(aes(color=RESISTANCE), size=5)+
		stat_ellipse(aes(x=PC1, y=PC2, color=RESISTANCE, group=RESISTANCE),type = "norm")+
		labs(x=paste("PC1 (", round(pV.PC1*100, 1), "% variance explained)", sep=""), y=paste("PC2 (", round(pV.PC2*100, 1), "% variance explained)", sep=""))+
		theme(text=element_text(size=20), axis.text=element_text(size=20), legend.position=c(0.89, 0.08))

jpeg("OUTPUT/PCA_biplot.jpeg", quality=100, width=1100, height=900)
bp
dev.off()

	#tabulating cross-resistants
df = data.frame(rownames(PCA.MERGE), RESISTANCE); colnames(df) = c("PLANT", "RESISTANCE")
df$POPUL = substr(df$PLANT, 1, 1)
HERBICIDE = rep(levels(df$RESISTANCE), each=2)
POPULATION = rep(c("INVERLEIGH", "URANA"), times=nlevels(df$RESISTANCE))
count = c()
for (r in levels(df$RESISTANCE)){
	for (p in c("i", "u")){
		d = subset(subset(df, RESISTANCE==r), POPUL==p)
		write.csv(d, file=paste("OUTPUT/Resistants_list-", r, "-", p, ".csv", sep=""))
		count = c(count, nrow(d))		
	}
}

R.pattern.stat = data.frame(POPULATION, HERBICIDE, count); R.pattern.stat$proportion = R.pattern.stat$count/sum(R.pattern.stat$count)
INV = subset(R.pattern.stat, POPULATION=="INVERLEIGH"); INV$proportion = INV$count/sum(INV$count)
URA = subset(R.pattern.stat, POPULATION=="URANA"); URA$proportion = URA$count/sum(URA$count)

write.csv(R.pattern.stat, file="OUTPUT/Resistance Pattern.csv")
write.csv(INV, file="OUTPUT/Resistance Pattern - INVERLEIGH.csv")
write.csv(URA, file="OUTPUT/Resistance Pattern - URANA.csv")

# #PCA all metrics:
# 	df2 = read.csv("OUTPUT/CV_DATA.csv")
# 	colnames(df) = c("PLANT", "RESISTANCE")
# 	df2 = merge(df, df2, by="PLANT")

# 	forPCA = scale(df2[,6:ncol(df2)], scale=TRUE, center=TRUE)
# 	PRCOMP = prcomp(forPCA)
# 	PC1 = PRCOMP$x[,1]; PC2 = PRCOMP$x[,2]	#Rotated data of the first 2 PC
# 	V = PRCOMP$sdev^2	#variance of each PC
# 	pV.PC1 = V[1]/sum(V); pV.PC2 = V[2]/sum(V)	#proportion of variance explained by the first 2 PC

# 	bp <- ggplot(data=data.frame(PC1, PC2), aes(PC1, PC2))+
# 			geom_point(aes(color=df2$RESISTANCE), size=5)+
# 			stat_ellipse(aes(x=PC1, y=PC2, color=df2$RESISTANCE, group=df2$RESISTANCE),type = "norm")+
# 			labs(x=paste("PC1 (", round(pV.PC1*100, 1), "% variance explained)", sep=""), y=paste("PC2 (", round(pV.PC2*100, 1), "% variance explained)", sep=""))+
# 			theme(text=element_text(size=20), axis.text=element_text(size=20), legend.position=c(0.89, 0.08))

# 	jpeg("OUTPUT/PCA_biplot.jpeg", quality=100, width=1100, height=900)
# 	bp
# 	dev.off()


#Structural Equation modelling
library(lme4)
# using the BLUEs instead of just means:
	DATA$QUANT.RES = (QUANT.RES - min(QUANT.RES))/(max(QUANT.RES) - min(QUANT.RES))
	# #transform back fo logit binary survival prediction via:
	# y = (DATA$QUANT.RES * (max(QUANT.RES) - min(QUANT.RES))) + min(QUANT.RES)
	# surv = 1/(1+exp(-y))

	#extracting BLUEs of each genotype per herbicide:
for(herbi in levels(DATA$HERBI)){
	blup.mod = lmer(QUANT.RES ~ 0 + (1|REPLI) + PLANT, data=subset(DATA, HERBI==herbi))
	blue = data.frame(fixef(blup.mod))
	rownames(blue) = gsub(pattern="PLANT", replacement="", rownames(blue))
	colnames(blue) = herbi
	eval(parse(text=paste(herbi, "=blue")))
}
m1 = merge(SULFO, TERBU, by="row.names"); rownames(m1) = m1$Row.names; m1 = m1[,2:3]
m2 = merge(GLYPH, TRIFL, by="row.names"); rownames(m2) = m2$Row.names; m2 = m2[,2:3]
MERGED = merge(m1, m2,  by="row.names")
colnames(MERGED) = c("PLANT", "SULFO", "TERBU", "GLYPH", "TRIFL")


#SEM.DAT = PCA.MERGE[,3:6]
SEM.DAT = MERGED[,2:5]
COV = cov(SEM.DAT)
	
	#regression + variance SEM
MOD.GLYPH=specifyModel()
	GLYPH<->GLYPH,VAR.gly,NA #variances
	SULFO<->SULFO,VAR.sul,NA
	TERBU<->TERBU,VAR.ter,NA
	TRIFL<->TRIFL,VAR.tri,NA
	GLYPH <-SULFO,REG.glysul,NA #gly
	GLYPH <-TERBU,REG.glyter,NA
	GLYPH <-TRIFL,REG.glytri,NA

fit.GLYPH = sem(MOD.GLYPH, COV, nrow(SEM.DAT))
summary(fit.GLYPH)

MOD.SULFO=specifyModel()
	GLYPH<->GLYPH,VAR.gly,NA #variances
	SULFO<->SULFO,VAR.sul,NA
	TERBU<->TERBU,VAR.ter,NA
	TRIFL<->TRIFL,VAR.tri,NA
	SULFO <-GLYPH,REG.sulgly,NA #sul
	SULFO <-TERBU,REG.sulter,NA
	SULFO <-TRIFL,REG.sultri,NA

fit.SULFO = sem(MOD.SULFO, COV, nrow(SEM.DAT))
summary(fit.SULFO)

MOD.TERBU=specifyModel()
	GLYPH<->GLYPH,VAR.gly,NA #variances
	SULFO<->SULFO,VAR.sul,NA
	TERBU<->TERBU,VAR.ter,NA
	TRIFL<->TRIFL,VAR.tri,NA
	TERBU <-SULFO,REG.tersul,NA #ter
	TERBU <-GLYPH,REG.tergly,NA
	TERBU <-TRIFL,REG.tertri,NA

fit.TERBU = sem(MOD.TERBU, COV, nrow(SEM.DAT))
summary(fit.TERBU)

MOD.TRIFL=specifyModel()
	GLYPH<->GLYPH,VAR.gly,NA #variances
	SULFO<->SULFO,VAR.sul,NA
	TERBU<->TERBU,VAR.ter,NA
	TRIFL<->TRIFL,VAR.tri,NA
	TRIFL <-SULFO,REG.trisul,NA #tri
	TRIFL <-TERBU,REG.triter,NA
	TRIFL <-GLYPH,REG.trigly,NA

fit.TRIFL = sem(MOD.TRIFL, COV, nrow(SEM.DAT))
summary(fit.TRIFL)

FITS = ls()[grep('fit', ls())]
lab=c(); est=c(); pval=c()
for (f in FITS){
	f = eval(parse(text=f))
	s = summary(f)$coef
	lab = c(lab, rownames(s)[5:7])
	est = c(est, standardizedCoefficients(f)[5:7,2])
	pval = c(pval, s[5:7, 4])
}

	#variance covariance SEM
MOD=specifyModel()
	GLYPH<->GLYPH,VAR.gly,NA #variances
	SULFO<->SULFO,VAR.sul,NA
	TERBU<->TERBU,VAR.ter,NA
	TRIFL<->TRIFL,VAR.tri,NA
	GLYPH<->SULFO,COV.glysul,NA #covariances
	GLYPH<->TERBU,COV.glyter,NA
	GLYPH<->TRIFL,COV.glytri,NA
	SULFO<->TERBU,COV.sulter,NA
	SULFO<->TRIFL,COV.sultri,NA
	TERBU<->TRIFL,COV.tertri,NA

fit = sem(MOD, COV, nrow(SEM.DAT))
summary(fit)
	f = fit
	s = summary(f)$coef
	lab = c(lab, rownames(s)[5:10])
	est = c(est, standardizedCoefficients(f)[5:10,2])
	pval = c(pval, s[5:10, 4])


SEM.OUT = data.frame(lab, est, pval)
write.csv(SEM.OUT, file="OUTPUT/SEM_regression.csv")

########################################################################

# MULTIVARITE BREEDER'S EQUATION FOR SIMULATING HERBICIDE CROSS-RESISTANCE DEVELOPMENT

#Structural Equation modelling
DAT = as.data.frame(cbind(PCA.MERGE[,1:2], scale(PCA.MERGE[,3:6], scale=T, center=T)))
#DAT = PCA.MERGE[,1:6]

jpeg("OUTPUT/Distribution of herbicide resistance", quality=100, width=900, height=900)
par(mfrow=c(2,2))
hist(DAT$GLYPH, ylab="Glyphosate Resistance`")
hist(DAT$SULFO, ylab="Sulfometuron Resistance")
hist(DAT$TERBU, ylab="Terbuthylazine Resistance")
hist(DAT$TRIFL, ylab="Trifluralin Resistance")
dev.off()

PROP.SEL = 0.05 #proportion of population selected (top 5%)
for (h in colnames(DAT)[3:6]){
	HERBI = eval(parse(text=paste("DAT$", h, sep="")))
	assign(paste("SEL.", h, sep=""), mean(HERBI[order(HERBI, decreasing=TRUE)][1:round(length(HERBI)*PROP.SEL)]) - mean(HERBI))
}

COV = cov(DAT[3:ncol(DAT)])
h2 = as.matrix(data.frame(GLY=0.4, SUL=0.8, TER=0.7, TRI=0.8))
h2 = sqrt(h2) #so that the heritabilities are eaqual to the one set above
h2= t(h2) %*% h2
G = as.matrix(COV * h2)
B = t(as.matrix(eval(parse(text=paste("data.frame(", paste(ls()[grep("SEL.", ls())], collapse=","), ")", sep="")))))
DELTA_Z = G%*%B #response to selection