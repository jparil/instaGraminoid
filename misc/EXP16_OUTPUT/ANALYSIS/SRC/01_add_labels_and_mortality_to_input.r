#!/usr/bin/env Rscript
#args = commandArgs(trailingOnly=TRUE)

# output=args[1]
# label=args[2]
setwd("/home/student.unimelb.edu.au/jparil/Documents/PHENOMICS - herbicide resistance leaf assays/instaGraminoid/MISC/EXP16_OUTPUT/ANALYSIS")
file.output="./INPUT/RAW/CONSOLIDATED-OUTPUT.csv"
file.lab="./INPUT/RAW/lab.csv"

#read instaGraminoid output csv file and arrange according to HERBICIDE then according to DAY
output = read.csv(file.output)
output = output[order(output$HERBI),]
output = output[order(output$DAY),]

#read genotype labels csv file and arrange according to HERBICIDE
lab = read.csv(file.lab)
lab$HERBICIDE = substr(lab$HERBICIDE, 1, 5)
lab = lab[order(lab$HERBICIDE),]
HERBI = rep(rep(lab$HERBICIDE, each=4), times=3)
POP = rep(rep(lab$POPULATION, each=4), times=3)
GENO = rep(rep(lab$GENOTYPE, each=4), times=3)
SURV = rep(rep(lab$SURVIVAL, each=4), times=3)
label = data.frame(HERBI, POP, GENO, SURV)

# check if the HERBICIDE columns match (merge is too computationally intensive for these large data frames)
check = output$HERBI==label$HERBI
if (sum(check)<nrow(output)) {
	print("Oh no! Something's wrong!")
} else {
	output$POP = label$POP
	output$GENO = label$GENO
	output$SURV = label$SURV
}

# renaming column names for 'cross_validation.r' analysis
DAY = output$DAY
POPUL = output$POP
PLANT = output$GENO
REPLI = output$REP
HERBI = output$HERBI
SURVI = output$SURV
METRICS = output[6:(ncol(output)-3)]

OUTPUT = data.frame(DAY, POPUL, PLANT, REPLI, HERBI, SURVI)
OUTPUT = cbind(OUTPUT, METRICS)

#remove CONTROL treatments to balance-out the data
OUTPUT = subset(OUTPUT, HERBI!="CONTR")

write.csv(OUTPUT, file="./INPUT/EXP16_ANALYSIS_INPUT.csv", row.names=F)
