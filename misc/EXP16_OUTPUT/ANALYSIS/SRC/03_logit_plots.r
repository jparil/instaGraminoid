################################################################################################
#load libraries
library(glmnet)
library(ROCR) #for the ROC plot
library(popbio) #for logit curve with histograms
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#

################################################################################################
#load input file
work_dir="/home/student.unimelb.edu.au/jparil/Documents/PHENOMICS - herbicide resistance leaf assays/instaGraminoid/misc/EXP16_OUTPUT/ANALYSIS"
data_filename="INPUT/EXP16_ANALYSIS_INPUT.csv"
setwd(work_dir)
dat = read.csv(data_filename)

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#

#generate the change in through time metrics
	ADDITIONAL_METRICS <- function(DATA, START_METRIC, END_METRIC) {
		#add metric transformations
			#DELTA
			DAYS = levels(dat$DAY)
			nDAYS = length(DAYS)
			METRICS = DATA[, START_METRIC:END_METRIC]
			nMETRICSPerDay = ncol(METRICS) / nDAYS
			for (i in 1:(nDAYS-1)) {
				for (j in (i+1):nDAYS) {
					DELTA = METRICS[, (((j-1)*nMETRICSPerDay)+1):(j*nMETRICSPerDay)] - METRICS[, (((i-1)*nMETRICSPerDay)+1):(i*nMETRICSPerDay)]
					NAMES = paste("DELTA", substr(DAYS[i],4,nchar(DAYS[i])), substr(DAYS[j],4,nchar(DAYS[j])), substr(colnames(DELTA), start=6, stop=nchar(colnames(DELTA))), sep="")
					colnames(DELTA) = NAMES
					DATA = cbind(DATA, DELTA)
				}
			}

			# #PAIRWISE PRODUCTS
			# METRICS = DATA[, START_METRIC:END_METRIC]
			# for (k in 1:(ncol(METRICS)-1)) {
			# 	for (l in (k+1):ncol(METRICS)) {
			# 		PROD = (METRICS[,k])/100 * METRICS[,l]
			# 		NAME = paste("PROD_", colnames(METRICS)[k], "_X_", colnames(METRICS)[l], sep="")
			# 		NAMES = c(colnames(DATA), NAME)
			# 		DATA = cbind(DATA, PROD)
			# 		colnames(DATA) = NAMES
			# 	}
			# }
		return(DATA)
	}

	dat = dat[order(dat$REPLI), ]
	dat = dat[order(dat$PLANT), ]
	dat = dat[order(dat$HERBI), ]
	dat = dat[order(dat$POPUL), ]
	dat = dat[order(dat$DAY), ]

	#isolate factors per measurement time
	ndays = nlevels(dat$DAY)
	POPUL = dat$POPUL[1:(nrow(dat)/ndays)]
	PLANT = dat$PLANT[1:(nrow(dat)/ndays)]
	REPLI = dat$REPLI[1:(nrow(dat)/ndays)]
	HERBI = dat$HERBI[1:(nrow(dat)/ndays)]
	SURVI = dat$SURVI[1:(nrow(dat)/ndays)]

	#merge into a new data set
	DATA = data.frame(POPUL, PLANT, REPLI, HERBI, SURVI)

	#decompose metrics per measurement time and cbind into the new data set
	DAYS = levels(dat$DAY)
	METRICS = colnames(dat)[7:ncol(dat)]
	for (i in DAYS) {
		SUBSET = subset(dat, DAY==i)
		for (j in METRICS) {
			eval(parse(text=paste("DATA$", i, "_", j, " = SUBSET$", j, sep="")))
		}
	}

	#add additional metrics
	DATA = ADDITIONAL_METRICS(DATA=DATA, START_METRIC=6, END_METRIC=ncol(DATA))

	#set infinite values as NA
	METRICS_ALL=colnames(DATA)[6:ncol(DATA)]
	for (m in METRICS_ALL) {
		eval(parse(text=paste("DATA$", m, "[is.infinite(DATA$", m, ")] = NA", sep="")))
	}

	#remove missing pots and columns filled with missing values and infinities, and standardize
	DATA = DATA[DATA$DAY00_AREA>0, ]	#remove empty pots at day 0
	DATA = DATA[, colSums(is.na(DATA))==0]
	#DATA[, 6:ncol(DATA)] = scale(DATA[, 6:ncol(DATA)], center=TRUE, scale=TRUE) #to scal or not to scale?!

	str(DATA)

	#save DATA for 05_resistance_variation.r use!
	write.csv(DATA, file="OUTPUT/CV_DATA.csv", row.names=FALSE)

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#

# #AIC + BIC criteria (minimize AIC and BIC)
	# 	#logistic model declaration
	# 	data=DATA[5:ncol(DATA)]
	# 	mod = glm(SURVI ~ ., data=data, family=binomial(link='logit'))

	# 	#step-wise model evaluation
	# 	step(mod)
	# 	# library(MASS)
	# 	# forward = stepAIC(mod, direction="forward")
	# 	# backward = stepAIC(mod, direction="backward")
	# 	# both = stepAIC(mod, direction="both")
	# 	# forward$anova
	# 	# backward$anova
	# 	# both$anova

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#

####################################
###			 					 ###
### MULTIPLE LOGISTIC REGRESSION ###
###								 ###
####################################
#logistic regression with multiple  predictors using ELASTIC-NET

	TRAINING_DATA=DATA
	VALIDATION_DATA=DATA
	training_name="ALL_POP_HER"
	validation_name="ALL_POP_HER"
	METRICS=colnames(DATA)[6:ncol(DATA)]

	#do not include:
	METRICS = METRICS[!grepl("DAY00", METRICS)]
	#METRICS = METRICS[!grepl("DAY", METRICS)]	#removed individual day measurements because they're highly correlated with the DELTA's which seem to be better as we're mneasuring the changes in the metric through time
	#METRICS = METRICS[!grepl("DELTA", METRICS)]

	ALPHA=0.5		#elasticnet mixing at half lasso and RR
	X_train = eval(parse(text=paste("cbind(", paste(paste("TRAINING_DATA$", METRICS, sep=""), collapse=","), ")", sep="")))
	colnames(X_train) = METRICS
	X_valid = eval(parse(text=paste("cbind(", paste(paste("VALIDATION_DATA$", METRICS, sep=""), collapse=","), ")", sep="")))
	colnames(X_valid) = METRICS
	MODEL = glmnet(x=X_train, y=as.factor(TRAINING_DATA$SURVI), family="binomial", alpha=ALPHA, nlambda=100)
	LAMBDAi = MODEL$dev.ratio==max(MODEL$dev.ratio)
		# WHICH PREDICTED VALUES TO USE AS THE QUANTITATIVE RESISTANCE ESTIMATE?
		# Just the Xb or the log-fitted 1/(1+exp(-Xb))???!!!
		P1 = MODEL$a0[LAMBDAi] + (X_valid%*%MODEL$beta[,LAMBDAi]) #more continuous than p(x) = 1/(1+exp(-(Xb)))
		P2 = 1/(1+exp(-(MODEL$a0[LAMBDAi] + (X_valid%*%MODEL$beta[,LAMBDAi])))) #almost binary since these are the fitted values!
		corr = cor.test(P1, P2)
		jpeg("OUTPUT/instaGraminoid_Xb_vs_logit(Xb).jpeg", quality=100, width=1000, height=1000)
		par(mfrow=c(2,2), cex=2, mar=c(5,5,1,1))
		hist(P1, xlab="Xb", main="")
		hist(P2, xlab="1/(1+exp(-Xb))", main="")
		df=data.frame(x=rep(c("Xb","1/(1+exp(-Xb))"), each=length(P1)), y=c(P1, P2)); boxplot(y~x, data=df)
		plot(P1, P2, xlab="Xb", ylab="1/(1+exp(-Xb)", sub=paste("COR = ", round(corr$estimate, 2), sep=""))
		dev.off()
		# I'll use Xb becuase it is more continous than the log-fitted values - AND I WANT A CONTINOUS VARIABLE FOR THE POOLING!!!
	QUANT.RES = (MODEL$a0[LAMBDAi] + (X_valid%*%MODEL$beta[,LAMBDAi]))[,1]
	PREDICTED = 1/(1+exp(-QUANT.RES))
	CLASSIFICATION = ifelse(PREDICTED >= 0.50, 1, 0) #we set the cut-off point at 0.50 - to find a better cut-off point implement cross-validation 10-fold?!
	CORRECT = sum(CLASSIFICATION==VALIDATION_DATA$SURVI)
	WRONG = sum(CLASSIFICATION!=VALIDATION_DATA$SURVI)
	PERFORMANCE = CORRECT/sum(CORRECT, WRONG)

	pr = prediction(PREDICTED, VALIDATION_DATA$SURVI)
	prf = performance(pr, measure="tpr", x.measure="fpr")
	auc = performance(pr, measure="auc")@y.values[[1]]

	colors = RColorBrewer::brewer.pal(10, name = "Set3")

	jpeg("OUTPUT/instaGraminoid_logit_ROC - GLMNET.jpeg", quality=100, width=2000, height=1000)
	par(mfrow=c(1, 2))
	par(cex=2.5)
	logi.hist.plot(QUANT.RES, VALIDATION_DATA$SURVI, boxp=F, type="hist", col=colors[5], xlabel="Quantitative Resistance Estimate", ylabel="Survival", mainlabel="Logistic Regression Plot")
	par(cex=2, cex.main=2, cex.axis=1.5, cex.lab=1.5, cex.sub=1.5, mar=c(7, 5, 5, 2))
	plot(prf, col="red", main="ROC Plot", lwd=5)
	legend(legend=paste("AUC = ", round(auc,2), sep=""), "bottomright")
	dev.off()

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#

# LOOKING AT THE PREDICTORS IN THE MULTIVARIATE LOGISTIC REGRESSION MODEL
	#observe the predictors sparsity and ranking
	str(MODEL)
	summary(MODEL)

	PREDICTORS = MODEL$beta[,LAMBDAi]
	PREDICTORS = as.data.frame(PREDICTORS[order(abs(PREDICTORS), decreasing=T)])
	PREDICTORS$CONTRIB = abs(PREDICTORS[,1]) / sum(abs(PREDICTORS[,1]))
	colnames(PREDICTORS) = c("VALUES", "CONTRIB")

	dim(PREDICTORS)

	boxplot(PREDICTORS)
	plot(density(unlist(PREDICTORS)))

	write.csv(PREDICTORS, file="OUTPUT/multivariate_logistic_regression_PREDICTORS.csv")
	write.csv(MODEL$a0[LAMBDAi], file="OUTPUT/MODEL_b0.csv")
	write.csv(MODEL$beta[,LAMBDAi], file="OUTPUT/MODEL_beta.csv")

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#

#########################################
###									  ###
### SINGLE METRIC LOGISTIC REGRESSION ###
###									  ###
#########################################

BEST_METRIC = eval(parse(text=paste("DATA", rownames(PREDICTORS)[1], sep="$")))
SURVI = DATA$SURVI

MODEL = glm(SURVI ~ BEST_METRIC, family=binomial(link='logit'))

plot(SURVI ~ BEST_METRIC, main="Logistic Regression Plot")
curve(predict(MODEL, data.frame(BEST_METRIC=x),type='resp'),add=TRUE,col=2)
points(BEST_METRIC, fitted(MODEL),pch=20,col=2)

colors = RColorBrewer::brewer.pal(10, name = "Set3")
logi.hist.plot(BEST_METRIC, SURVI, boxp=F, type="hist", col=colors[5], xlab="BEST_METRIC")

#QUANT.RES = MODEL$coef[1] + MODEL$coef[2]*BEST_METRIC
PREDICTED = 1/(1+exp(-(MODEL$coef[1] + MODEL$coef[2]*BEST_METRIC)))
CLASSIFICATION = ifelse(PREDICTED > 0.50, 1, 0) #we set the cut-off point at 0.50 - to find a better cut-off point implement cross-validation 10-fold?!
CORRECT = sum(CLASSIFICATION==SURVI)
WRONG = sum(CLASSIFICATION!=SURVI)
PERFORMANCE = CORRECT/sum(CORRECT, WRONG)

pr = prediction(PREDICTED, SURVI)
prf = performance(pr, measure="tpr", x.measure="fpr")
auc = performance(pr, measure="auc")@y.values[[1]]
plot(prf, col="red", main="ROC Plot", sub=paste("AUC = ", round(auc,2), sep=""))

#saving plots
jpeg("OUTPUT/instaGraminoid_logit_ROC.jpeg", quality=100, width=2000, height=1000)
par(mfrow=c(1, 2))
par(cex=2, cex.main=2, cex.axis=1.5, cex.lab=1.5, cex.sub=1.5, mar=c(7, 5, 5, 2))
logi.hist.plot(BEST_METRIC, SURVI, boxp=F, type="hist", col=colors[5], xlabel="Quantitative Resistance Estimate", ylabel="Survival", mainlabel="Logistic Regression Plot")
plot(prf, col="red", main="ROC Plot", lwd=5)
legend(legend=paste("AUC = ", round(auc,2), sep=""), "bottomright")
dev.off()
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
