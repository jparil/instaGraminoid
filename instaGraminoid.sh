#!/bin/bash
package=instaGraminoid

###########################
###						###
### Arguments handling: ###
###						###
###########################
if [ $# == 0 ]; then
	echo "No arguments passed. Try again dude."
	echo " "
fi

while test $# -gt 0
do
	case "$1" in
		-h|--help)
			echo " "
			echo " "
			echo "$package - a suite of Bash, python, and R scripts"
			echo "for extracting plant health-related metrics from plant photographs."
			echo "Pre-requisite: git clone the repository into the home directory, i.e."
			echo "		$ cd ~"
			echo "		$ git clone https://github.com/jeffersonfparil/instaGraminoid.git"
			echo " "
			echo "Arguments:"
			echo "-h, --help		show brief help"
			echo "-a, --ACTION=		0 for colorimetric analysis of photographs,"
			echo "			1 for logistic regression cross-validation, and for final quantitative plant health metric extraction"
			echo "-d, --directory=DIR	specify the directory where:"
			echo "						for ACTION=0: the images are located (full path: e.g. -d /home/ubuntu/instaGraminoid/test)"
			echo "						for ACTION=1: the output directory (full path: e.g. -d /home/ubuntu/instaGraminoid/test/OUTPUT)"
			echo "-p, 			specify additional parameters:"
			echo "	FOR ACTION=0:"
			echo "			i) specify the extension name of the images (e.g. jpg, tiff  and png)"
			echo "			ii) SET_WHITE - white pixel value of background: 0 (dark) - 255 (pure white)"
			echo "			iii) THRESHOLD - plant detection threshold: 0 (less) - 255 (more) | detect features < threshold"
			echo "	FOR ACTION=1:"
			echo "			i) dataframe of ACTION=0 output merged with the empirical survival data"
			echo "			ii) analysis to perform:"
			echo "				a) 1 = cross-validation"
			echo "				b) 2 = logistic regression plots"
			echo "				c) 3 = quantitative resistance estimates + resistance variations"
			echo " "
			echo "Examples:"
			echo "nohup ./instaGraminoid.sh -a 0 -d ~/instaGraminoid/test -p jpg 200 150 &"
			echo "nohup ./instaGraminoid.sh -a 1 -d ~/instaGraminoid/test/OUTPUT -p ~/instaGraminoid/test/SAMPLE_INPUT.csv 1 &"
			echo " "
			echo " "
			echo "Usage of miscellaneous scripts:"
			echo "./rename_pictures.sh /working/directory/ 07 JPG"
			echo "./identify_plants.r /output/directory/ OUTPUT.csv SURVIVAL.csv 4 42 3 4 2 2"
			echo "		- refer to the scripts for more details."
			echo " "
			echo " "
			echo "Notes:"
			echo "Checks for the following dependencies and installs them (This may take a while. Sorry.):"
			echo "		- python, python-pip, opencv-python, numpy, matplotlib and scipy"
			echo "		- R, agricolae, glmnet and ROCR"
			echo "		- parallel"
			echo "Make sure you have updated your system's package repository list e.g. sudo apt update"
			echo " "
			echo " "
			exit 0
			;;
		-a)
			shift
			if test $# -gt 0; then
				export ACTION=$1
			else
				echo "no action specified"
				exit 1
			fi
			shift
			;;
		--action*)
			export ACTION=`echo $1 | sed -e 's/^[^=]*=//g'`
			shift
			;;
		-d)
			shift
			if test $# -gt 0; then
				export DIR=$1
			else
				echo "no input directory specified"
				exit 1
			fi
			shift
			;;
		--directory*)
			export DIR=`echo $1 | sed -e 's/^[^=]*=//g'`
			shift
			;;
		-p)
			shift
			if test $# -eq 2; then
				export m1=$1
				export m2=$2
				echo "Selected ACTION=1"
			elif test $# -eq 3; then
				export m1=$1
				export m2=$2
				export m3=$3
				echo "Selected ACTION=0"
			else
				echo "Missing some parameters"
				exit 1
			fi
			shift
			;;
		*)
		break
		;;
	esac
done
#gnildnah stnemugrA

########################################################################################################################
########################################################################################################################
########################################################################################################################

################
###			 ###
### CHECK OS ###
###			 ###
################
case "$(uname -s)" in
	Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:$(uname -s)"
esac

#then add cases below for when machine==c(Linux, Mac, the new windows subsystem for linux, etc..?)

########################################
###									 ###
### CHECK FOR REQUIRED DEPENDENCIES: ###
###									 ###
########################################
# check for python, python-pip and r-base in Linux (or windows subsytem for linux) and Mac
if [ $machine = Linux ]; then
	for p in python pip R parallel; do
		if hash $p &>/dev/null; then
			echo "$p exists!"
		else
			echo "$p does not exist!"
			if [ $p = pip ]; then sudo apt install python-pip python-tk
			elif [ $p = R ]; then sudo apt install r-base
			else sudo apt install $p
			fi
		fi
	done
# elif [ $machine = Mac ]; then
	# #install brew (press enter and allow)
	# /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	# #install python, pip, R and parallel via brew
	# for p in python pip R parallel; do
	# 	if hash $p &>/dev/null; then
	# 		echo "$p exists!"
	# 	else
	# 		echo "$p does not exist!"
	# 		# if [ $p = pip ]; then brew install python-pip
	# 		if [ $p = pip ]; then sudo easy_install pip
	# 		elif [ $p = R ]; then brew install r
	# 		else brew install $p
	# 		fi
	# 	fi
	# done
else
	echo 'Your system is not yet fully supported.'
	echo 'Please use our standalone executables found in https://gitlab.com/jeffersonfparil/instagraminoid_executables'
	echo '==============================================================================================================='
	echo ""
	exit 1
fi
# install python modules: opencv-python, numpy, matplotlib, scipy and seaborn
for m in opencv-python numpy matplotlib scipy seaborn; do
	if [ $m = opencv-python ]; then
		if [ $(python -c "import pkgutil; print(1 if pkgutil.find_loader('cv2') else 0)") = 0 ]; then pip install --user opencv-python; fi
	else
		if [ $(python -c "import pkgutil; print(1 if pkgutil.find_loader('$m') else 0)") = 0 ]; then pip install --user $m; fi
	fi
done
# install R packages:
echo 'This may take a while. Sorry.'
R -e 'dir.create(path = Sys.getenv("R_LIBS_USER"), showWarnings = FALSE, recursive = TRUE)'
echo -e "args = commandArgs(trailingOnly=TRUE)" > installRpackages.r
echo -e "PKG=args[1]; if(!(require(PKG, character.only=T))) {install.packages(PKG, dep=T, repos='http://cran.us.r-project.org')}" >> installRpackages.r
for l in glmnet agricolae ROCR ggplot2 lme4 sem; do
	Rscript installRpackages.r $l
done
rm installRpackages.r

########################################################################################################################
########################################################################################################################
########################################################################################################################

if [ $ACTION -eq 0 ]; then
	###################
	###				###
	###	 DEBUGGING	### + HEAVY MODIFICATIONS 20180306
	###				###
	################### 2018 02 19
	# DIR=/mnt/MISC0_OPENCV_PICTURE_ANALYSIS/
	# DIR=/volume1/MISC0_OPENCV_PICTURE_ANALYSIS/
	# m1=jpg
	# m2=200
	# m3=150

	mkdir ${DIR}/OUTPUT/; mkdir ${DIR}/OUTPUT/IMAGES/

	cd ${DIR}
	find | grep .${m1} | sort > images.temp

	#allow for native python parallelization by setting the maximum number of jobs in parallel to half of the total number of cores available
	JOBS=$(echo $(nproc)/2 | bc)

	#run metric extraction in parallel
	parallel -j$JOBS ~/instaGraminoid/openCV_plant_colorimetric.py {} $m2 $m3 ::: $(cat images.temp)
	#INPUTS:
		#01) image filename
		#02) white background value (0(dark)-255(pure white))
		#03) plant detection threshold (0(less)-255(more): pixel values < threshold are detected: smaller pixel values means darker)
	#OUTPUTS:
		#01) metrics file: OUTPUT.csv
		#02) background subtracted plant pictures
		#03) BGR channel histograms per plant picture (turned-off by default; acivate inside the script: line 191)

	#concatenate individual output csv files into one csv file: OUPUT.csv
	mv *.csv OUTPUT/; mv *-out.${m1} OUTPUT/IMAGES/; OUTPUT/IMAGES/

	cd OUTPUT/
	ls *-out.csv > outputList.temp
	echo -e "DAY,TRAY,CELL,REP,AREA,BLUE,GREEN,RED,GREEN_INDEX,GREEN_FRACTION,AUC_BLUE,AUC_GREEN,AUC_RED,THRESH_GREEN1,THRESH_GREEN2,THRESH_RED1,THRESH_RED2,MEDIAN_BLUE,MEDIAN_GREEN,MEDIAN_RED" > OUTPUT.csv
	for out in $(cat outputList.temp)
	do
	tail -n1 $out >> OUTPUT.csv
	echo $out
	done

	sed -i 's/-/,/g' OUTPUT.csv

	# clean up and organizing
	rm *.temp
	mkdir CSV
	mv *-out.csv CSV/
	
elif [ $ACTION -eq 1 ]; then
	#extract the final output, or perform cross-validation and generate logistic regression plots
	#INPUTS:
		#01) data file where the photograph-based metrics are merged with the sulrvival data (PATH/filename)
		#02) output directory (must exist)
		#03) analysis to perform:
				# 1 = cross-validation across populations and herbicides (test the transferrability of the logistic model)
				# 2 = logistic regression plots per herbicide per population
				# 3 = extract quantitative resistance estimates + description of cross-resistance and resistance variation
	#OUPUT
		#01) Respective, tables and,
		#02) Graphs
	if [ $m2 -eq 1 ]; then
		Rscript ~/instaGraminoid/cross_validation.r "$m1" "$DIR"
	elif [ $m2 -eq 2 ]; then
		Rscript ~/instaGraminoid/logit_plots.r "$m1" "$DIR"
	elif [ $m2 -eq 3 ]; then
		Rscript ~/instaGraminoid/resistance_variation.r "$m1" "$DIR"
	else
		echo "INVALID ANALYSIS TYPE. CHOOSE:"
		echo "	1 for cross-validation,"
		echo "	2 for logistic regression plots and,"
		echo "	3 for final qunatitative plant health metric extraction."
		echo " "
	fi

else
	echo "INVALID ACTION. CHOOSE:"
	echo "	0 for openCV colorimetric analysis, while"
	echo "	1 for logistic regression cross-validation and final qunatitative plant health metric extraction."
	echo " "
fi

########################################################################################################################
########################################################################################################################
########################################################################################################################
