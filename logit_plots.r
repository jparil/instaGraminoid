#!/usr/bin/env Rscript

#plot logistic regressions for each herbicide treatment per population

#usage example: $ Rscript logit_plots.r "$(pwd)"/res/SAMPLE/INPUT/SAMPLE_INPUT.csv "$(pwd)"/res/SAMPLE/OUTPUT

#input
args = commandArgs(trailing = TRUE)
input_file = args[1] #input csv file with the instaGraminoid-derived metrics and the binary survival data merged into 1 datframe
output_dir = args[2] #output directory where to write-out all outputs

#setup libraries and quantitative_resistance_estimate extraction script
library(glmnet)
library(lme4)
library(ROCR)
source("quantitative_resistance.r")

#import data
dat = read.csv(input_file)

#set output directory
setwd(output_dir)

#estimate herbicide resistance as a continuous phenotype using the instaGraminoid photograph-based metrics modelled as the explanatory variables for the binary survival data
DATA = QUANTITATIVE_RESISTANCE_ESTIMATE(dat=dat)

#logistic regression!
colors = RColorBrewer::brewer.pal(10, name = "Set3")
for (popul in levels(DATA$POPUL)){
	for (herbi in levels(DATA$HERBI)){
		data = subset(subset(DATA, POPUL==popul), HERBI==herbi)
			jpeg(paste0("Logistic Regression-", popul, "-", herbi, ".jpg"), quality=100, width=1000, height=1000)
			par(cex=3, mar=c(2,2,1,1))
			if(herbi=="SULFO"){col=colors[5]} else if(herbi=="TERBU"){col=colors[4]} else if(herbi=="GLYPH"){col=colors[2]} else {col=colors[1]}
			survi = data$SURVI
			quant.res = data$QUANT.RES
			plot(survi ~ quant.res, xlim=c(0,1), ylim=c(0,1), type="n")
			#plot histograms (snippets from https://rdrr.io/cran/popbio/src/R/logi.hist.plot.R)
				independ = quant.res; depend = survi
				scale.hist = 5; col.hist = col; count.hist=TRUE; intervalo = 0; las.h1 = 1
				h.br <- hist(independ, plot = FALSE)$br
				if (intervalo > 0) h.br <- seq(from = range(h.br)[1], to = range(h.br)[2], by = intervalo)
				h.x <- hist(independ[depend == 0], breaks = h.br, plot = FALSE)$mid

				# get counts in each bin
				h.y0 <- hist(independ[depend == 0], breaks = h.br, plot = FALSE)$counts
				h.y1 <- hist(independ[depend == 1], breaks = h.br, plot = FALSE)$counts

				# scale the histogram bars to max desired length:
				h.y0n <- h.y0/(max(c(h.y0,h.y1))* scale.hist)
				h.y1n <- 1 - h.y1/(max(c(h.y0,h.y1))* scale.hist)

				# draw bottom histogram:
				for (i in 1:length(h.y0n)){
				if (h.y0n[i] > 0)
				polygon(c(rep(h.br[i], 2), rep(h.br[i+1], 2)), c(0, rep(h.y0n[i], 2), 0), col = col.hist)
				}

				# draw top histogram:
				for (i in 1:length(h.y1n)){
				if (h.y1n[i] < 1)
				polygon(c(rep(h.br[i], 2), rep(h.br[i+1], 2)),
				c(h.y1n[i], 1, 1, h.y1n[i]), col = col.hist)
				}
			#add logit curve
				model=glm(survi ~ quant.res, family=binomial(link="logit"))
				curve(predict(model, data.frame(quant.res=x),type='resp'),add=TRUE,col=2, lwd=5)
			auc = tryCatch(performance(prediction(data$PRED, data$SURVI), measure="auc")@y.values[[1]], error=function(e){auc=1})
			legend(x=0, y=0.9, legend=paste0("ROC AUC=", round(auc*100, 0), "%"))
			dev.off()
	}
}
